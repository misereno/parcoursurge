echo "Download the docker images from gitlab..."
docker-compose -f docker-compose-gitlab.yml pull

echo "Mount back/tmp for the data already parsed OR to put the data once parsed..."
echo "Mount back/data for the data to parsed if not already..."
echo "Launch the corresponding docker containers..."
docker-compose -f docker-compose-gitlab.yml up

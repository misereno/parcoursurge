import fetch from 'node-fetch';
import https from 'https';
import { mount, createLocalVue } from '@vue/test-utils';

import Vuetify from 'vuetify';
import Vuex from 'vuex';
import Vue from 'vue';

import { getters } from "@/plugins/vuex";
import App from '@/App.vue';
import MainView from '@/components/MainView.vue';
import SequenceView from '@/components/SequenceView.vue';
import { ComparisonMode, ViewType } from "@/Config.js";

Vue.use(Vuetify);
Vue.use(Vuex)

const localVue = createLocalVue();

describe('App.vue', () => {
  let actions;
  let state;
  let store;
  let vuetify;
  let wrapper = null;

  beforeEach(() => {
    state = {
      view_type: ViewType.MAIN_VIEW_TYPE,
      history: {undo: jest.fn(), redo: jest.fn()},
      full_tree: {total_count: 10, nb_processed: 10, filtered_count: 5},
      comparison_mode: ComparisonMode.NONE_COMPARISON_MODE,
    };
    actions = {
      initApp: jest.fn(),
    };
    store = new Vuex.Store({
      actions,
      getters,
      state,
    });
    vuetify = new Vuetify();
  });

  afterEach(() => {
    if(wrapper != null)
      wrapper.destroy();
    wrapper = null;
  });

  function wrapperFactory() {
    //Stub our components that we do not want to load because of the store
    //We could also use "shallowMount", but then, objects would be "object-stub" instead of "object" or ".object" (for vuetify)
    let stubs = {MainView: true, SequenceView: true, SideView: true};
    const wrapper = mount(App, {store, localVue, vuetify, stubs});
    return wrapper;
  }

  test("Init", () => {
    wrapper = wrapperFactory();
    expect(actions.initApp).toHaveBeenCalled();
  });

  test("Component Visible", async () => {
    wrapper            = wrapperFactory();
    const mainView     = wrapper.findComponent(MainView);
    const sequenceView = wrapper.findComponent(SequenceView);

    //Check that components exists
    expect(mainView.exists()).toBe(true);
    expect(sequenceView.exists()).toBe(true);

    //Check that sequenceView is hidden
    expect(mainView.element.style.display).not.toBe("none");
    expect(sequenceView.element.style.display).toBe("none");

    //Switch the view type and check that mainView is hidden
    store.state.view_type = ViewType.SEQUENCE_VIEW_TYPE;
    await localVue.nextTick();
    expect(mainView.element.style.display).toBe("none");
    expect(sequenceView.element.style.display).not.toBe("none");
  });

  test("Link not dead", async () => {
    wrapper     = wrapperFactory();
    const vBtns = wrapper.findAll(".v-btn");

    expect(vBtns.wrappers.length).toBeGreaterThan(0);

    //Used to authorize bad certificate HTTPS request
    const httpsAgent = new https.Agent({
      rejectUnauthorized: false,
    });
    for(let i=0; i < vBtns.wrappers.length; i+=1) {
      let btn = vBtns.wrappers[i];
      let href = btn.element.getAttribute('href');
      if(!href || !href.startsWith("http")) //If no link: do nothing
        continue;

      //If link: check that this link is valid
      let res = await fetch(href, { method: 'GET', agent: httpsAgent});
      expect(res.status).toBe(200);
    }
  });

  test("Undo/Redo", async () => {
    wrapper = wrapperFactory();

    //Undo
    document.dispatchEvent(new KeyboardEvent('keyup', {key: 'z', ctrlKey: true}));
    expect(state.history.undo).toHaveBeenCalledTimes(1);
    expect(state.history.redo).toHaveBeenCalledTimes(0);

    //Redo
    document.dispatchEvent(new KeyboardEvent('keyup', {key: 'y', ctrlKey: true}));
    expect(state.history.undo).toHaveBeenCalledTimes(1);
    expect(state.history.redo).toHaveBeenCalledTimes(1);
  });
});

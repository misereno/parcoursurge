import { mount, createLocalVue } from '@vue/test-utils';

import Vuetify from 'vuetify';
import Vuex from 'vuex';
import Vue from 'vue';

import { getters } from "@/plugins/vuex";
import SideView from '@/components/SideView.vue';
import { ViewType, TabView } from '@/Config';

import DetailPanel   from "@/components/Sidebar/Detail.vue";
import AttrsPanel    from "@/components/Sidebar/Attributes.vue";
import OverviewPanel from "@/components/Sidebar/Overview.vue";
import HistoryPanel  from "@/components/Sidebar/History.vue";

Vue.use(Vuetify);
Vue.use(Vuex)

const localVue = createLocalVue();

describe('SideView.vue', () => {
  let state
  let actions;

  let store;
  let vuetify;

  beforeEach(() => {
    state = {
      tab: TabView.OVERVIEW_TAB,
      display: {side: {height: 1024}},
      view_type: ViewType.MAIN_VIEW_TYPE,
      current_sequence: [],
      align_sequence_by: 0,
    };
    actions = {
      set_hidden_items: jest.fn(),
      set_align_sequence: jest.fn(),
    };
    store = new Vuex.Store({
      state,
      actions,
      getters,
    });
    vuetify = new Vuetify();
  });

  function wrapperFactory(store) {
    //Stub our components that we do not want to load because of the store
    //We could also use "shallowMount", but then, objects would be "object-stub" instead of "object" or ".object" (for vuetify)
    let stubs = {DetailPanel: true, AttrsPanel : true, OverviewPanel : true, HistoryPanel : true};

    const wrapper = mount(SideView, {store, localVue, vuetify, stubs});
    return wrapper;
  }

  test("Tab", async () => {
    const wrapper = wrapperFactory(store);
    await wrapper.vm.$nextTick();
    await new Promise(resolve => setTimeout(resolve, 1)); //Issue with transitions...  need to wait

    const detailPanel   = wrapper.findComponent(DetailPanel);
    const attrsPanel    = wrapper.findComponent(AttrsPanel);
    const overviewPanel = wrapper.findComponent(OverviewPanel);
    const historyPanel  = wrapper.findComponent(HistoryPanel);

    //Test initialization
    expect(detailPanel.exists()).toBe(false);
    expect(attrsPanel.exists()).toBe(false);
    expect(overviewPanel.exists()).toBe(true);
    expect(historyPanel.exists()).toBe(false);

    //Test the change of all tabs
    let tabIDs = [TabView.OVERVIEW_TAB, TabView.DETAIL_TAB, TabView.CONTROL_TAB, TabView.HISTORY_TAB];
    let panels = [OverviewPanel, DetailPanel, AttrsPanel, HistoryPanel];
    let component = null;

    for(let i = 0; i < tabIDs.length; i++) {
      state.tab = tabIDs[i];
      await wrapper.vm.$nextTick();
      await new Promise(resolve => setTimeout(resolve, 1)); //Issue with transitions...  need to wait
      for(let j = 0; j < tabIDs.length; j++) {
        component = wrapper.findComponent(panels[j]);
        if(j == i){
          expect(component.exists()).toBe(true);
          expect(component.isVisible()).toBeTruthy();
        }
        else if(component.exists()) {
          expect(component.isVisible()).toBeFalsy();
        }
      }
    }
  });

  test("AlignBy", async () => {
    const wrapper = wrapperFactory(store);
    expect(wrapper.find("#alignBy").exists()).toBeFalsy();

    state.current_sequence = ["phyto", "fivealpha"];
    await wrapper.vm.$nextTick();
    expect(wrapper.find("#alignBy").isVisible()).toBeTruthy();

    let sequenceDOM = wrapper.findAll(".in_sequence");
    expect(sequenceDOM.length).toBe(2); //phyto and fivealpha
    
    //Click on "phyto". Only "fivealpha" should remain
    for(let i = 0; i < sequenceDOM.length; i++) {
      if(sequenceDOM.at(i).text() == "phyto") {
        await sequenceDOM.at(i).trigger("click");
        break;
      }
    }
    expect(state.current_sequence.length).toBe(1);
    expect(state.current_sequence[0]).toBe("fivealpha");

    //Align by fivealpha
    await wrapper.find("#alignBy").trigger("click");
    expect(actions.set_align_sequence).toHaveBeenCalledTimes(1);
    expect(actions.set_align_sequence.mock.calls[0][1].sequence).toBe(state.current_sequence);
    expect(actions.set_align_sequence.mock.calls[0][1].nOccurence).toBe(0);
    state.view_type = ViewType.SEQUENCE_VIEW_TYPE;
    await wrapper.vm.$nextTick();

    //Cancel alignement should appear, and the current_sequence should remain
    sequenceDOM = wrapper.findAll(".in_sequence");
    expect(sequenceDOM.length).toBe(1); //fivealpha
    expect(sequenceDOM.at(0).text()).toBe("fivealpha");
    expect(state.current_sequence.length).toBe(1);
    expect(state.current_sequence[0]).toBe("fivealpha");

    expect(wrapper.find("#cancel").isVisible()).toBeTruthy();
    
    //Exit cancel alignement
    await wrapper.find("#cancel").trigger("click");
    expect(actions.set_align_sequence).toHaveBeenCalledTimes(2);
    expect(actions.set_align_sequence.mock.calls[1][1].sequence.length).toBe(0);
    expect(actions.set_align_sequence.mock.calls[1][1].nOccurence).toBe(0);
  });

  test("Occurence", async () => {
    const wrapper = wrapperFactory(store);
    expect(wrapper.find("#alignByID").exists()).toBeFalsy();
    expect(wrapper.find("#checkboxLast").exists()).toBeFalsy();

    state.current_sequence = ["fivealpha"];
    await wrapper.vm.$nextTick();

    //Change the alignBy value (test negative and positive values)
    let alignByID = wrapper.find("#alignByID");
    let checkboxLast = wrapper.find("#checkboxLast");
    expect(alignByID.isVisible()).toBeTruthy();
    expect(checkboxLast.exists()).toBeTruthy();
    alignByID.element.value = -1;
    await new Promise(resolve => setTimeout(resolve, 1)); //Issue with async stuff
    expect(alignByID.element.value).toBe("1");

    alignByID.element.value = 2;
    alignByID.trigger("input");
    await new Promise(resolve => setTimeout(resolve, 1)); //Issue with async stuff
    expect(alignByID.element.value).toBe("2");

    //Check that the "alignByID - 1" is taken into consideration
    expect(wrapper.find("#alignBy").isVisible()).toBeTruthy();
    await wrapper.find("#alignBy").trigger("click");
    expect(actions.set_align_sequence).toHaveBeenCalledTimes(1);
    expect(actions.set_align_sequence.mock.calls[0][1].sequence).toBe(state.current_sequence);
    expect(actions.set_align_sequence.mock.calls[0][1].nOccurence).toBe(1); //alignByID - 1
    
    //Check the checkbox
    checkboxLast.element.checked = true;
    checkboxLast.trigger("change");
    expect(wrapper.find("#alignBy").isVisible()).toBeTruthy();
    await wrapper.find("#alignBy").trigger("click");
    expect(actions.set_align_sequence).toHaveBeenCalledTimes(2); //One more time
    expect(actions.set_align_sequence.mock.calls[1][1].sequence).toBe(state.current_sequence);
    expect(actions.set_align_sequence.mock.calls[1][1].nOccurence).toBe(-1); //Last is checked --> nOccurence == -1

    
    //Check the callback with the store
    state.align_sequence_by = 3;
    await wrapper.vm.$nextTick();
    expect(alignByID.element.value).toBe("4"); //align_sequence_by + 1
    expect(alignByID.element.disabled).toBeFalsy();
    expect(checkboxLast.element.checked).toBeFalsy();
    
    state.align_sequence_by = -1;
    await wrapper.vm.$nextTick();
    expect(alignByID.element.disabled).toBeTruthy();
    expect(checkboxLast.element.checked).toBeTruthy();
  });
});

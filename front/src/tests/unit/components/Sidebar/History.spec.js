import { mount, createLocalVue } from '@vue/test-utils';

import Vuetify from 'vuetify';
import Vuex from 'vuex';
import Vue from 'vue';

import { getters } from "@/plugins/vuex";
import HistoryView from "@/components/Sidebar/History.vue";
import FileModal from "@/modals/FileModal";
import { History, HistoryNode } from "@/History";
import { ComparisonMode } from "@/Config.js";

import * as d3 from "d3";

Vue.use(Vuetify);
Vue.use(Vuex);

//Needed because getBBox is not defined in jest...
Object.defineProperty(global.SVGElement.prototype, 'getBBox', {
  writable: true,
  value: jest.fn().mockReturnValue({
    x: 0,
    y: 0,
    width:  0,
    height: 0,
  }),
});

const localVue = createLocalVue();

//Count the number of visible nodes from a svg
//@param svg the svg containing all the history visualization
//@return the number of visible nodes on screen
function countVisibleNodes(svg) {
  let nVisible = 0;
  let nodes = svg.selectAll(".tree > .node");
  for(const node of nodes)
    if(node.style.display != "none")
      nVisible++;
  return nVisible;
}

function isNodeOrParentOf(parentNode, childNode) {
  for(let curNode = childNode; curNode != null; curNode = curNode.parent)
      if(curNode == parentNode)
          return true;
  return false;
}

//Create a new object "History". See "@/tests/unit/History.spec.js" for more details
//@return a new object History that should look like: 
//root --> child1
//     --> child2 --> child 3
//                --> child 4
function createHistory() {
  let history = new History(new HistoryNode("root", {}, "root"));
  let childNode = new HistoryNode("type", {}, "child1");
  history.insertNode(childNode, false);

  let absoluteChildNode = new HistoryNode("filter", {}, "child2");
  history.insertNode(absoluteChildNode, true);

  let childNode3 = new HistoryNode("zoomIn", {}, "child3");
  history.insertNode(childNode3, false);

  let childNode4 = new HistoryNode("filter", {}, "child4");
  history.insertNode(childNode4, true);

  return history;
}

describe('History.vue', () => {
  let state
  let actions;

  let store;
  let vuetify;

  beforeEach(() => {
    d3.selection.prototype.duration   = function(){ return this }
    d3.selection.prototype.transition = function(){ return this }

    state = {
      history: createHistory(),
      display: {
                 side: {width: 480}, 
                 main: {innerHeight: 1024},
               },
      comparison_mode: ComparisonMode.NONE_COMPARISON_MODE,
      comparison_nodes: null,
    };
    actions = {
      set_history: jest.fn(),
      start_new_comparison: jest.fn(),
    };
    store = new Vuex.Store({
      state,
      actions,
      getters,
    });
    vuetify = new Vuetify();
  });

  function wrapperFactory() {
    let stubs = {FileModal: false};
    const wrapper = mount(HistoryView, {store, localVue, vuetify, stubs});
    return wrapper;
  }

  test("FileModal", async () => {
    const wrapper = wrapperFactory();
    await wrapper.vm.$nextTick();

    let fileModalComponent = wrapper.findComponent(FileModal);
    expect(fileModalComponent.exists()).toBe(true);
    expect(fileModalComponent.html()).toBe(""); //Should be empty as it is not displayed...
    
    //Start to load files...
    await wrapper.find(".load").trigger("click");
    await wrapper.vm.$nextTick();
    expect(fileModalComponent.html()).not.toBe(""); //Should be visible now
    
    //Cancel the loading
    var event = new KeyboardEvent('keydown', {'keyCode': 27}); //escape
    document.dispatchEvent(event);
    await wrapper.vm.$nextTick();
    expect(fileModalComponent.html()).toBe(""); //Should be hidden again
  });

  test("ExportJSON", async() => {
    const wrapper = wrapperFactory();
    global.URL.createObjectURL = jest.fn();
    await wrapper.find(".save").trigger("click");

    expect(global.URL.createObjectURL).toHaveBeenCalledTimes(1);
    expect(global.URL.createObjectURL.mock.calls[0][0].type).toBe("application/json;charset=utf-8");
    expect(global.URL.createObjectURL.mock.calls[0][0].size).toBe(state.history.toString().length); //As we cannot get the Blob data here, we just compare the size of the Blob with what we expect. History.toString is tested in @/tests/unit/History.spec.js
  });

  test("Graph layout", async () => {
    const wrapper = wrapperFactory();
    let svg = d3.select(wrapper.find("svg").element);
    let nodes = svg.selectAll(".tree > .node");

    expect(nodes.size()).toBe(5); //We expect five nodes
    let findNode = (n) => {
      return nodes.filter(function(d) { return d.data == n; });
    };

    let root   = findNode(state.history.tree).nodes()[0];
    let child1 = findNode(state.history.tree.children[0]).nodes()[0];
    let child2 = findNode(state.history.tree.children[1]).nodes()[0];
    let child3 = findNode(state.history.tree.children[1].children[0]).nodes()[0];
    let child4 = findNode(state.history.tree.children[1].children[1]).nodes()[0];

    expect(root.__data__.x).toBeLessThan(child1.__data__.x);
    expect(child1.__data__.x).toBe(child2.__data__.x);
    expect(child1.__data__.x).toBeLessThan(child3.__data__.x);
    expect(child4.__data__.x).toBe(child3.__data__.x);

    //With a linear structure, everything should be within the same page
    state.history.tree.children = [state.history.tree.children[1]]; //Keep only child2
    state.history.tree.children[0].children = [state.history.tree.children[0].children[0]]; //Keep only child3
    state.history.curNode = state.history.tree;
    //structure: root -> child2 -> child3
    await wrapper.vm.$nextTick();
    nodes = svg.selectAll(".tree > .node");
    root   = findNode(state.history.tree).nodes()[0];
    child2 = findNode(state.history.tree.children[0]).nodes()[0];
    child3 = findNode(state.history.tree.children[0].children[0]).nodes()[0];

    expect(nodes.size()).toBe(3); //We expect three nodes as we deleted two
    expect(root.__data__.x).toBe(child2.__data__.x);
    expect(root.__data__.x).toBe(child3.__data__.x);
  });

  test("Graph_extend", async () => {
    const wrapper = wrapperFactory();
    let svg = d3.select(wrapper.find("svg").element);

    expect(countVisibleNodes(svg)).toBe(5); //All nodes should be visible

    state.history.tree.children[1].extend = false; //Hide everything in child2
    await Vue.nextTick();
    expect(countVisibleNodes(svg)).toBe(3); //We expect three visible nodes
    
    state.history.tree.extend = false; //Hide everything from root, except root
    await Vue.nextTick();
    expect(countVisibleNodes(svg)).toBe(1); //Only root should be visible
    
    //Unhide root. Child2 is still "not extended"
    state.history.tree.extend = true
    await Vue.nextTick();
    expect(countVisibleNodes(svg)).toBe(3); //We expect three visible nodes
  });

  test("Graph compare_to", async () => {
    const wrapper = wrapperFactory();
    let svg = d3.select(wrapper.find("svg").element);

    await wrapper.find(".toggleComparison").trigger("click");
    expect(svg.selectAll(".tree > .node.comparison").size()).toBe(4); //We expect five nodes. We can compare the current state (one node) to the other available nodes (four others)
  });

  test("Graph_click", async () => {
    const wrapper = wrapperFactory();
    let svg = d3.select(wrapper.find("svg").element);

    //Try to find the node "child1" to "click" on it.
    let selection = svg.selectAll(".tree > .node");
    let child1Node = null;
    for(const node of selection) {
      if(node.__data__.data == state.history.tree.children[0]){
        child1Node = node;
        break;
      }
    }
    expect(child1Node).not.toBeNull();

    //Check that if we click on the root node will change the status of the history object
    d3.select(child1Node).select("circle").node().dispatchEvent(new Event('click'));
    await wrapper.vm.$nextTick();
    expect(state.history.curNode).toBe(state.history.tree.children[0]);
    expect(svg.select(".tree > .node.selected").node().__data__.data).toBe(state.history.tree.children[0]);
  });

  test("Graph Bookmark", async () => {
    const wrapper = wrapperFactory();
    state.history.curNode = state.history.tree;
    await wrapper.find(".showBookmark").vm.$emit("change", true);
    await wrapper.vm.$nextTick();

    let svg = d3.select(wrapper.find("svg").element);

    //only root should be visible at this point
    expect(countVisibleNodes(svg)).toBe(1); 
    let nodes = svg.selectAll(".tree > .node");
    for(const node of nodes)
      if(node.style.display != "none")
        expect(node.__data__.data).toBe(state.history.tree);

    //Root and child2 should be visible ONLY
    state.history.tree.children[1].bookmarked = true;
    await wrapper.vm.$nextTick();
    expect(countVisibleNodes(svg)).toBe(2); //root, child2, child3
    for(const node of nodes)
      if(node.style.display != "none")
        expect([state.history.tree, state.history.tree.children[1]]).toContain(node.__data__.data);

    //If we advance deeper in the tree, all the nodes from root and curNode should be visible
    state.history.tree.children[1].bookmarked = false;
    state.history.curNode = state.history.tree.children[1].children[0]; //child3
    await wrapper.vm.$nextTick();
    expect(countVisibleNodes(svg)).toBe(3); //root, child2, child3
    for(const node of nodes)
      if(node.style.display != "none")
        expect(isNodeOrParentOf(node.__data__.data, state.history.curNode)).toBe(true);

    //If we bookmarked two grandchildren (but not necessarily within the same depth in the data space), then their depth (in the visualization space) should be similar
    state.history.tree.children[0].bookmarked             = true;
    state.history.tree.children[1].children[0].bookmarked = true;
    state.history.curNode = state.history.tree;
    await wrapper.vm.$nextTick();

    expect(countVisibleNodes(svg)).toBe(3); //root, child1, child3
    let root   = null;
    let child1 = null;
    let child3 = null;
    for(const node of nodes) {
      if(node.style.display != "none") {
        if(node.__data__.data == state.history.tree.children[0])
          child1 = node;
        else if(node.__data__.data == state.history.tree.children[1].children[0])
          child3 = node;
        else if(node.__data__.data == state.history.tree)
          root = node;
      }
    }

    expect(root).toBeTruthy();
    expect(child1).toBeTruthy();
    expect(child3).toBeTruthy();
    expect(child1.__data__.x).toBe(child3.__data__.x);
    expect(child1.__data__.x).toBeGreaterThan(root.__data__.x);
  });

  test("Comparison", async () => {
    const wrapper = wrapperFactory();
    await wrapper.find(".toggleComparison").trigger("click");

    let svg = d3.select(wrapper.find("svg").element);

    //Try to find the root node to "click" on it.
    let selection = svg.selectAll(".tree > .node");

    let child1Node = null;
    let searchSVGNode = (dataNode) => {
      for(const node of selection) {
        if(node.__data__.data == dataNode)
          return node;
      }
    };
    child1Node = searchSVGNode(state.history.tree.children[0]);
    expect(child1Node).not.toBeNull();

    //Click on child1Node
    d3.select(child1Node).select("circle").node().dispatchEvent(new Event('click'));
    await wrapper.vm.$nextTick();

    //Check that the function start_new_comparison has been called correctly
    expect(actions.start_new_comparison).toHaveBeenCalledTimes(1);
    expect(actions.start_new_comparison).toHaveBeenLastCalledWith(expect.anything(), state.history.tree.children[0]);

    //Check the class while performing a comparison...
    state.comparison_nodes = [state.history.tree, state.history.tree.children[0]];
    let sourceNode = searchSVGNode(state.comparison_nodes[0]);
    let targetNode = searchSVGNode(state.comparison_nodes[1]);
    expect(sourceNode).toBeTruthy();
    expect(targetNode).toBeTruthy();

    //...With target
    state.comparison_mode  = ComparisonMode.TARGET_COMPARISON_MODE;
    state.history.curNode  = state.comparison_nodes[1];
    await wrapper.vm.$nextTick();
    expect(targetNode).toBe(svg.select(".tree > .node.selected").node());
    expect(sourceNode).toBe(svg.select(".tree > .node.comparison").node());

    //...With source
    state.comparison_mode  = ComparisonMode.SOURCE_COMPARISON_MODE;
    state.history.curNode  = state.comparison_nodes[0];
    await wrapper.vm.$nextTick();
    expect(sourceNode).toBe(svg.select(".tree > .node.selected").node());
    expect(targetNode).toBe(svg.select(".tree > .node.comparison").node());

    //Stop the comparison
    state.comparison_mode = ComparisonMode.NONE_COMPARISON_MODE;
    await wrapper.vm.$nextTick();
    expect(sourceNode).toBe(svg.select(".tree > .node.selected").node());
    expect(svg.select(".tree > .node.comparison").node()).toBeNull();
  });

  test("Undo/redo", async() => {
    const wrapper = wrapperFactory();
    let undoBtn = wrapper.find(".undo");
    let redoBtn = wrapper.find(".redo");

    let curNode = state.history.curNode;

    expect(undoBtn.vm.$props['disabled']).not.toBe(state.history.canUndo);
    expect(redoBtn.vm.$props['disabled']).not.toBe(state.history.canRedo);

    //Undo
    await undoBtn.trigger("click");
    expect(state.history.curNode).toBe(curNode.parent);
    expect(redoBtn.vm.$props['disabled']).toBeFalsy();

    //Redo
    await redoBtn.trigger("click");
    expect(redoBtn.vm.$props['disabled']).toBeTruthy();
    expect(state.history.curNode).toBe(curNode);
  });
});

import { mount, createLocalVue } from '@vue/test-utils';

import Vuetify from 'vuetify';
import Vuex from 'vuex';
import Vue from 'vue';

import { getters, defaultDistribution } from "@/plugins/vuex";
import Detail        from "@/components/Sidebar/Detail.vue";
import HistogramView from "@/components/HistogramView.vue";
import BarchartView  from "@/components/BarchartView.vue";

Vue.use(Vuetify);
Vue.use(Vuex);

const localVue = createLocalVue();

describe('Detail.vue', () => {
  let state

  let store;
  let vuetify;

  beforeEach(() => {
    state = {
      distribution: defaultDistribution(),
      display: {
                 side: {width: 480}, 
                 main: {innerHeight: 1024},
               },
      diseases: [
          { name: "Diabete",      color: "#8c510a" },
          { name: "Hypertension", color: "#f6e8c3" }
      ],
      requested: {color: "#0000ff"},
    };
    store = new Vuex.Store({
      state,
      getters,
    });
    vuetify = new Vuetify();
  });

  function wrapperFactory() {
    let stubs = {HistogramView: false, BarchartView: false};
    const wrapper = mount(Detail, {store, localVue, vuetify, stubs});
    return wrapper;
  }

  test("Visibility", async () => {
    const wrapper = wrapperFactory();
    await Vue.nextTick();

    //Test default visibility
    expect(wrapper.find("#emptyMessage").exists()).toBeTruthy();
    expect(wrapper.find("#distribution_age").exists()).toBeFalsy();
    expect(wrapper.find("#distribution_duration").exists()).toBeFalsy();
    expect(wrapper.find("#distribution_disease").exists()).toBeFalsy();

    state.distribution.age = {
       bins: [4, 2, 3, 6],
       range: [0, 15]
    };
    await Vue.nextTick();
    expect(wrapper.find("#emptyMessage").exists()).toBeFalsy();
    expect(wrapper.find("#distribution_age").exists()).toBeTruthy();
    expect(wrapper.findComponent(HistogramView).exists()).toBeTruthy();
    state.distribution.age = {};


    state.distribution.duration = {
       bins: [4, 2, 3, 6],
       range: [0, 15]
    };
    await Vue.nextTick();
    expect(wrapper.find("#emptyMessage").exists()).toBeFalsy();
    expect(wrapper.find("#distribution_age").exists()).toBeFalsy();
    expect(wrapper.find("#distribution_duration").exists()).toBeTruthy();
    expect(wrapper.findComponent(HistogramView).exists()).toBeTruthy();
    state.distribution.duration = {};


    state.distribution.disease = [
      {name: "Hypertension", value: 0},
      {name: "Diabete", value: 10},
    ];
    state.distribution.nbValues = 100;
    await Vue.nextTick();
    expect(wrapper.find("#emptyMessage").exists()).toBeFalsy();
    expect(wrapper.find("#distribution_duration").exists()).toBeFalsy();
    expect(wrapper.find("#distribution_disease").exists()).toBeTruthy();
    expect(wrapper.findComponent(BarchartView).exists()).toBeTruthy();
    state.distribution.disease = {};
  });
});

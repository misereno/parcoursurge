import { mount, createLocalVue } from '@vue/test-utils';

import Vuetify from 'vuetify';
import Vuex from 'vuex';
import Vue from 'vue';

import { getters } from "@/plugins/vuex";
import Overview from '@/components/Sidebar/Overview.vue';
import { generateMainViewTree, generateSequenceViewTree, generateEmptySequenceView } from '../../utils';
import { TreeToDisplay, ViewType } from "@/Config.js";

Vue.use(Vuetify);
Vue.use(Vuex)

const localVue = createLocalVue();


describe('Overview.vue', () => {
  let state
  let actions;

  let store;
  let vuetify;

  beforeEach(() => {
    state = {
      current_tree: null,
      view_type: ViewType.MAIN_VIEW_TYPE,
      sequence_view_curtree_to_display: TreeToDisplay.BEFORE,
      main_view_zoomed_node_id: 0,
      sequence_view_zoomed_nodes_ids: [0, 0],
      filter: {
        hidden_items: [],
      },
      display: { side: {width: 100, height: 50} }
    };
    actions = {
      set_hidden_items: jest.fn(),
    };
    store = new Vuex.Store({
      state,
      actions,
      getters,
    });
    vuetify = new Vuetify();
  });

  function wrapperFactory(store) {
    const wrapper = mount(Overview, {store, localVue, vuetify});
    return wrapper;
  }

  let testUnzoomedMainTree = async (wrapper) => {
    expect(wrapper.vm.$data.items.length).toBe(3); //alphabloc + surgery + fivealpha phyto
                                                   //Root should be discarded as it is part of DiscardEntries
    
    //Verify that they are sorted
    for(let i = 1; i < wrapper.vm.$data.items.length; i++)
      expect(wrapper.vm.$data.items[i-1].value).toBeGreaterThan(wrapper.vm.$data.items[i].value);

    //Verify that the numbers and names are right
    expect(wrapper.vm.$data.items[0].name).toBe("alphabloc");
    expect(wrapper.vm.$data.items[1].name).toBe("surgery");
    expect(wrapper.vm.$data.items[2].name).toBe("fivealpha phyto");

    expect(wrapper.vm.$data.items[0].value).toBeCloseTo(50); //Percentages
    expect(wrapper.vm.$data.items[1].value).toBeCloseTo(30); //Percentages
    expect(wrapper.vm.$data.items[2].value).toBeCloseTo(20); //Percentages
   
    //Verify that everything is in
    await Vue.nextTick();
    let components = wrapper.findAll(".v-list-item-group .v-list-item");
    expect(components.length).toBe(3);


    //Click on each component -> Disable everything
    for(let i = 0; i < components.length; i++) {
      components.wrappers[i].trigger("click");
      await Vue.nextTick();
      expect(actions.set_hidden_items).toHaveBeenLastCalledWith(expect.anything(), wrapper.vm.$data.items.slice(0, i+1).map((item) => item.name));
    }

    //Renable everything
    for(let i = 0; i < components.length; i++) {
      components.wrappers[i].trigger("click");
      await Vue.nextTick();
      expect(actions.set_hidden_items).toHaveBeenLastCalledWith(expect.anything(), wrapper.vm.$data.items.slice(i+1).map((item) => item.name));
    }

    //Click on each component -> Disable everything again to test the change of hidden_items when items changes as well.
    for(let i = 0; i < components.length; i++){
      components.wrappers[i].trigger("click");
      await Vue.nextTick();
    }
    state.filter.hidden_items = actions.set_hidden_items.mock.calls[actions.set_hidden_items.mock.calls.length-1][1];
  };

  test("MainView_simple", async () => {
    let tree = generateMainViewTree();
    store.state.current_tree = tree;
    const wrapper = wrapperFactory(store);

    await testUnzoomedMainTree(wrapper);
  });

  test("MainView_Zoom", async () => {
    let tree = generateMainViewTree();
    store.state.current_tree = tree;
    const wrapper = wrapperFactory(store);
    await testUnzoomedMainTree(wrapper);

    store.state.main_view_zoomed_node_id = 1; //zoom in the first node.

    //Only alphabloc should remain and be counted in
    await Vue.nextTick();
    expect(wrapper.vm.$data.items.length).toBe(1); 
    expect(wrapper.vm.$data.items[0].name).toBe("alphabloc");
    expect(wrapper.vm.$data.items[0].value).toBeCloseTo(100);
    expect(wrapper.findAll(".v-list-item-group .v-list-item").at(0).classes()).toContain('v-item--active');

    store.state.main_view_zoomed_node_id = 0; //unzoom to test the v-item--active stuff
    await Vue.nextTick();
    expect(wrapper.findAll(".v-list-item-group .v-list-item").at(0).classes()).toContain('v-item--active'); //As alphabloc is the 'highest' event, it should be first in the array
    expect(wrapper.findAll(".v-list-item-group .v-list-item").at(1).classes()).toContain('v-item--active');
    expect(wrapper.findAll(".v-list-item-group .v-list-item").at(2).classes()).toContain('v-item--active');

    //Zoom to a leaf. As we reached a leaf, nothing should remain in the list of items...
    //Indeed, the current node should not be counted in the total
    store.state.main_view_zoomed_node_id = 2;
    await Vue.nextTick();
    expect(wrapper.vm.$data.items.length).toBe(0); 
    expect(wrapper.findAll(".v-list-item-group .v-list-item").length).toBe(0);
  });

  test("SequenceView", async () => {
    let tree = generateSequenceViewTree();
    store.state.current_tree = tree;
    store.state.view_type = ViewType.SEQUENCE_VIEW_TYPE;
    store.state.sequence_view_curtree_to_display = TreeToDisplay.BEFORE;
    const wrapper = wrapperFactory(store);

    await testUnzoomedMainTree(wrapper);

    //As we reached a leaf, nothing should remain in the list of items...
    //Indeed, the current node should not be counted in the total
    store.state.sequence_view_zoomed_nodes_ids = [4, 8];
    await Vue.nextTick();
    expect(wrapper.vm.$data.items.length).toBe(0); 
    expect(wrapper.findAll(".v-list-item-group .v-list-item").length).toBe(0);

    let checkBeforeNode3 = async() => {
      //Same as in MainView_Zoom, only alphabloc should remain
      await Vue.nextTick();
      expect(wrapper.vm.$data.items.length).toBe(1); 
      expect(wrapper.vm.$data.items[0].name).toBe("alphabloc");
      expect(wrapper.vm.$data.items[0].value).toBe(100);
      expect(wrapper.findAll(".v-list-item-group .v-list-item").length).toBe(1);
    };
    store.state.sequence_view_zoomed_nodes_ids = [3, 8]; //zoom in the first node of before.
    await checkBeforeNode3();

    //Switch to after on which  we have zoomed on node == 8
    store.state.sequence_view_curtree_to_display = TreeToDisplay.AFTER;
    await Vue.nextTick();
    expect(wrapper.vm.$data.items.length).toBe(1); 
    expect(wrapper.vm.$data.items[0].name).toBe("phyto");
    expect(wrapper.vm.$data.items[0].value).toBeCloseTo(100);
    expect(wrapper.findAll(".v-list-item-group .v-list-item").length).toBe(1);

    //If we go back to "Before", we should have the previous behavior
    store.state.sequence_view_curtree_to_display = TreeToDisplay.BEFORE;
    await checkBeforeNode3();
  });

  test("Empty Sequence View", async () => {
    //Start from normal tree where v-list-item-group are > 0
    let tree = generateMainViewTree();
    store.state.current_tree = tree;
    const wrapper = wrapperFactory(store);
    await Vue.nextTick();
    expect(wrapper.findAll(".v-list-item-group .v-list-item").length).toBeGreaterThan(1);

    //Switch to an empty tree
    //Before tree...
    store.state.current_tree = generateEmptySequenceView();
    store.state.view_type = ViewType.SEQUENCE_VIEW_TYPE;
    store.state.sequence_view_curtree_to_display = TreeToDisplay.BEFORE;
    await Vue.nextTick();
    expect(wrapper.findAll(".v-list-item-group .v-list-item").length).toBe(0);

    //and after tree...
    store.state.sequence_view_curtree_to_display = TreeToDisplay.AFTER;
    await Vue.nextTick();
    expect(wrapper.findAll(".v-list-item-group .v-list-item").length).toBe(0);
  });
});

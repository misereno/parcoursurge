import { shallowMount } from '@vue/test-utils';
import Vue from 'vue';
import * as d3 from 'd3';
import IcicleTreeView from '@/components/IcicleTreeView.vue'
import { color_map, is_color_dark } from '@/color';

const data = {
  "type_combination": 0,
  "nb_occurences": 68337,
  "name": "root",
  "children": [
    {
      "type_combination": 128,
      "nb_occurences": 57142,
      "name": "medication",
      "children": [
        {
          "type_combination": 2,
          "nb_occurences": 27483,
          "name": "alphabloc"
        },
        {
          "type_combination": 4,
          "nb_occurences": 13597,
          "name": "phyto"
        },
        {
          "type_combination": 6,
          "nb_occurences": 6573,
          "name": "alphabloc phyto"
        },
        {
          "type_combination": 3,
          "nb_occurences": 3888,
          "name": "fivealpha alphabloc"
        },
        {
          "type_combination": 7,
          "nb_occurences": 686,
          "name": "fivealpha alphabloc phyto"
        },
        {
          "type_combination": 1,
          "nb_occurences": 4915,
          "name": "fivealpha"
        }
      ]
    },
    {
      "type_combination": 256,
      "nb_occurences": 5864,
      "name": "pause",
      "children": [
        {
          "type_combination": 64,
          "nb_occurences": 3089,
          "name": "interruption"
        },
        {
          "type_combination": 32,
          "nb_occurences": 2775,
          "name": "no treatment"
        }
      ]
    },
    {
      "type_combination": 8,
      "nb_occurences": 5291,
      "name": "surgery"
    },
    {
      "type_combination": 16,
      "nb_occurences": 40,
      "name": "death"
    }
  ]
};
function wrapperFactory() {
  const wrapper = shallowMount(IcicleTreeView, {
                               propsData: {
                                 height: 300,
                                 width: 300,
                                 is_color_dark: is_color_dark,
                                 color_map: color_map,
                                 padding: [0, 0, 0, 0],
                                 data: data,
                               }
                             }
  );
  return wrapper;
}

describe("IcicleTreeView", () => {
  test("structure", () => {
    const wrapper = wrapperFactory();

    let svg = d3.select(wrapper.vm.$refs['main']);
    let d3Nodes = svg.selectAll("g");

    function checkStructure_rec(node) {
      if(node.children == undefined)
        node.children = []; //Create a default array of children just in case

      let d3CurNode = d3Nodes.filter((d) => d.data.type_combination == node.type_combination).select("rect");

      //Check that all children are BELOW their parent
      for(const child of node.children) {
        let d3CurChild = d3Nodes.filter((d) => d.data.type_combination == child.type_combination).select("rect");
        
        expect(parseFloat(d3CurChild.attr("y"))).toBeCloseTo(parseFloat(d3CurNode.attr("y")) + parseFloat(d3CurNode.attr("height")));

        checkStructure_rec(child);
      }

      //Check that all children are NEXT TO EACH OTHER
      for(let i = 0; i < node.children.length-1; i++) {
        let d3CurChild_0 = d3Nodes.filter((d) => d.data.type_combination == node.children[i].type_combination).select("rect");
        let d3CurChild_1 = d3Nodes.filter((d) => d.data.type_combination == node.children[i+1].type_combination).select("rect");

        expect(parseFloat(d3CurChild_0.attr("y"))).toBe(parseFloat(d3CurChild_1.attr("y")));
        expect(parseFloat(d3CurChild_0.attr("x")) + parseFloat(d3CurChild_0.attr("width"))).toBeCloseTo(parseFloat(d3CurChild_1.attr("x")));
      }
    }

    //Root is always hidden
    expect(d3Nodes.filter((d) => d.data.type_combination == data.type_combination).node()).toBeNull();

    for(const child of data.children) {
      checkStructure_rec(child);
    }
  });

  test("remove data", async () => {
    const wrapper = wrapperFactory();

    wrapper.setProps({data: null}); 
    await Vue.nextTick();

    expect(d3.select(wrapper.vm.$refs['main']).selectAll("g").nodes().length).toBe(0);
  });
});

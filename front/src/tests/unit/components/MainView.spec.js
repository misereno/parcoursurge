import { mount, createLocalVue } from '@vue/test-utils';

import Vuetify from 'vuetify';
import Vuex from 'vuex';
import Vue from 'vue';

import * as d3 from "d3";
import * as treeUtils from "@/treeUtils";

import { getters } from "@/plugins/vuex";
import MainView from '@/components/MainView.vue';
import { ViewType } from '@/Config';
import { parseTransform, generateMainViewTree } from '../utils';

Vue.use(Vuetify);
Vue.use(Vuex)

const localVue = createLocalVue();

const d3SelectionProto = Object.assign({}, d3.selection.prototype);

describe('MainView.vue', () => {

  let state
  let actions;
  let mutations;

  let store;
  let vuetify;

  beforeEach(() => {
    //Disable animation of D3. Indeed, issue with transitions (due to JSDOM) otherwise for unit testing ONLY, see https://stackoverflow.com/questions/14443724/disabling-all-d3-animations-for-testing
    d3.selection.prototype.duration   = function(){ return this }
    d3.selection.prototype.transition = function(){ return this }
    d3.selection.prototype.on = function(event, callback) {
      if (event === 'start' || event === 'end') {
        this.each(callback);
        return this;
      } else {
        return d3SelectionProto.on.apply(this, arguments);
      }
    };

    state = {
      view_type               : ViewType.MAIN_VIEW_TYPE,
      current_tree            : null,
      display                 : {
                                  main: {
                                    width:  1920, 
                                    height: 1080, 
                                    padding: {
                                      left: 0, 
                                      top: 0, 
                                      right: 0, 
                                      bottom: 0
                                    },
                                    innerWidth: 1920, 
                                    innerHeight: 1080,
                                  }
                                },
      display_flag            : 0,
      main_view_zoomed_node_id: 0,
    };
    actions = {
      get_distribution:             jest.fn(),
      set_main_view_zoomed_node_id: jest.fn(),
    };
    mutations = {
      set_current_sequence: jest.fn(),
      set_tab: jest.fn(),
    };
    store = new Vuex.Store({
      state,
      actions,
      mutations,
      getters,
    });
    vuetify = new Vuetify();

    jest.useRealTimers();
  });

  function wrapperFactory() {
    const wrapper = mount(MainView, {store, localVue, vuetify});
    return wrapper;
  }

  test("Init", async () => {
    const wrapper = wrapperFactory();
    expect(wrapper.find("#mainViewZoomInfo").exists()).toBeTruthy();

    let svg = d3.select(wrapper.vm.$refs['view']);
    expect(svg.select(".content").selectAll("rect").empty()).toBeTruthy();
  });

  test("Nb Rect", async () => {
    state.current_tree = generateMainViewTree();
    const wrapper = wrapperFactory();

    let svg = d3.select(wrapper.vm.$refs['view']);
    expect(svg.select(".content").selectAll("rect").size()).toBe(5);
  });

  test("Hierarchy X", async() => {
    state.current_tree = generateMainViewTree();
    const wrapper = wrapperFactory();

    let svg = d3.select(wrapper.vm.$refs['view']);
    let nbParents = 0;
    svg.select(".content").selectAll("rect").each(function(data, _ind) {
      //Check that children nodes are always placed RIGHT to parents nodes
      if(data.parent != null) {
        nbParents++;
        let parentNode = svg.select(".content").selectAll("rect").filter((d) => (d.data.children.find((c) => c.id == data.data.id) != null));
        let parentX     = parseFloat(parseTransform(parentNode.attr('transform')).translate[0]); 
        let parentWidth = parseFloat(parentNode.attr('width')); 
        let childX      = parseFloat(parseTransform(d3.select(this).attr('transform')).translate[0]); 

        expect(parentX + parentWidth).toBeLessThanOrEqual(childX);
      }
    });

    expect(nbParents).toBe(4); //Total nodes minus root
  });

  test("Hierarchy Y", async() => {
    state.current_tree = generateMainViewTree();
    const wrapper = wrapperFactory();

    let svg = d3.select(wrapper.vm.$refs['view']);
    let nodes = svg.select(".content").selectAll("rect");

    function checkYRec(curNode) {
      for(let i = 0; i < curNode.children.length-1; i++) {
        let nD3      = nodes.filter((d) => d.data.id == curNode.children[i].id);
        let nHeight  = parseFloat(nD3.attr('height'));
        let nY       = parseFloat(parseTransform(nD3.attr('transform')).translate[1]);
        let nPlus1_Y = parseFloat(parseTransform(nodes.filter((d) => d.data.id == curNode.children[i+1].id).attr('transform')).translate[1]);
        expect(nY + nHeight).toBeLessThanOrEqual(nPlus1_Y); 
        checkYRec(curNode.children[i]);
      }
      if(curNode.children.length > 0)
        checkYRec(curNode.children[curNode.children.length-1]);
    }

    checkYRec(state.current_tree);
  });

  test("Click", async () => {
    state.current_tree = generateMainViewTree();
    const wrapper = wrapperFactory();

    let svg   = d3.select(wrapper.vm.$refs['view']);
    let nodes = svg.select(".content").selectAll("rect");

    function checkClickNode_rec(node, nthCall, parentPrefix) {
      let d3Node = nodes.filter((d) => d.data.id == node.id);
      d3Node.node().dispatchEvent(new Event('click'));
      nthCall++;
      expect(svg.select(".content").selectAll(".selectedNode").size()).toBe(1);
      expect(svg.select(".content").selectAll(".selectedNode").node()).toBe(d3Node.node());
      expect(mutations.set_current_sequence).toHaveBeenCalledTimes(nthCall);
      expect(mutations.set_tab).toHaveBeenCalledTimes(nthCall);
      expect(mutations.set_current_sequence).toHaveBeenLastCalledWith(expect.anything(), [...parentPrefix, node.name]);
      expect(actions.get_distribution).toHaveBeenCalledTimes(nthCall);
      expect(actions.get_distribution.mock.calls[nthCall-1][1].action).toBe("distribution");
      expect(actions.get_distribution.mock.calls[nthCall-1][1].node_id).toBe(node.id);
      for(let i = 0; i < node.children.length; i++)
        nthCall = checkClickNode_rec(node.children[i], nthCall, [...parentPrefix, node.name]);

      return nthCall;
    }

    let nthCall = 0;
    for(let i = 0; i < state.current_tree.children.length; i++)
      nthCall = checkClickNode_rec(state.current_tree.children[i], nthCall, []);
    expect(nthCall).toBe(4); //all minus root

    //Zoom on something
    let newD3Root = nodes.filter((d) => d.data.id == 3);
    newD3Root.node().dispatchEvent(new Event('dblclick'));
    expect(actions.set_main_view_zoomed_node_id).toHaveBeenCalledTimes(1);
    expect(actions.set_main_view_zoomed_node_id).toHaveBeenLastCalledWith(expect.anything(), {id: 3});
    state.main_view_zoomed_node_id = 3;
    await Vue.nextTick();

    //Check zoom_in prefix for a leaf AFTER root (size == 0)
    expect(wrapper.find("#mainViewZoomInfo").exists()).toBeTruthy();
    expect(wrapper.findAll("#mainViewZoomInfo .zoomInSpan").length).toBe(1);
    expect(wrapper.findAll("#mainViewZoomInfo .zoomInSpan").at(0).text("")).toBe("surgery");
    
    //...check that all are visible
    nodes = d3.select(wrapper.vm.$refs['view']).select(".content").selectAll("rect");
    expect(nodes.size()).toBe(2);
    
    //...and check again the prefix
    let newRoot = state.current_tree.children[1];
    nthCall = checkClickNode_rec(newRoot, nthCall, []);
    expect(nthCall).toBe(2+4); //Two more calls
    
    //Zoom on a leaf
    newD3Root = nodes.filter((d) => d.data.id == 4);
    newD3Root.node().dispatchEvent(new Event('dblclick'));
    expect(actions.set_main_view_zoomed_node_id).toHaveBeenCalledTimes(2);
    expect(actions.set_main_view_zoomed_node_id).toHaveBeenLastCalledWith(expect.anything(), {id: 4});
    state.main_view_zoomed_node_id = 4;
    await Vue.nextTick();

    //Check zoom_in prefix for a leaf AFTER root (size == 0)
    expect(wrapper.find("#mainViewZoomInfo").exists()).toBeTruthy();
    expect(wrapper.findAll("#mainViewZoomInfo .zoomInSpan").length).toBe(2);
    expect(wrapper.findAll("#mainViewZoomInfo .zoomInSpan").at(0).text()).toBe("surgery");
    expect(wrapper.findAll("#mainViewZoomInfo .zoomInSpan").at(1).text()).toBe("fivealpha phyto");
    
    //...check that all are visible
    nodes = d3.select(wrapper.vm.$refs['view']).select(".content").selectAll("rect");
    expect(nodes.size()).toBe(1); //Show only the leaf

    //...and check again the prefix
    //However, this leaf was "clicked" before. We need to "unclick" it
    nodes.node().dispatchEvent(new Event('click'));
    nthCall++;
    expect(svg.select(".content").selectAll(".selectedNode").size()).toBe(0);
    newRoot = state.current_tree.children[1].children[0];
    nthCall = checkClickNode_rec(newRoot, nthCall, [state.current_tree.children[1].name]);
    expect(nthCall).toBe(1+7); //One more calls
    
    //Unzoom
    svg.node().dispatchEvent(new Event('dblclick'));
    expect(actions.set_main_view_zoomed_node_id).toHaveBeenCalledTimes(3);
    expect(actions.set_main_view_zoomed_node_id).toHaveBeenLastCalledWith(expect.anything(), {id: -1});
    state.main_view_zoomed_node_id = -1;
    await Vue.nextTick();

    //Check zoom_in prefix for unzoomed view
    expect(wrapper.find("#mainViewZoomInfo").exists()).toBeTruthy();
    expect(wrapper.findAll("#mainViewZoomInfo .zoomInSpan").length).toBe(0);

    //Unzooming again does nothing
    svg.node().dispatchEvent(new Event('dblclick'));
    expect(actions.set_main_view_zoomed_node_id).toHaveBeenCalledTimes(3);
  });

  test("Wrong view type", async () => {
    state.current_tree = generateMainViewTree();
    state.view_type = ViewType.MAIN_VIEW_TYPE;
    const wrapper = wrapperFactory();

    expect(wrapper.isVisible()).toBeTruthy();
    let svg = d3.select(wrapper.vm.$refs['view']);
    expect(svg.select(".content").selectAll("rect").empty()).toBeFalsy();

    state.view_type = ViewType.SEQUENCE_VIEW_TYPE;
    await Vue.nextTick();
    await new Promise(resolve => setTimeout(resolve, 1));
    expect(wrapper.isVisible()).toBeFalsy();
  });

  test("Empty tree", async () => {
    jest.useFakeTimers();
    state.current_tree = generateMainViewTree();
    const wrapper = wrapperFactory();

    let svg = d3.select(wrapper.vm.$refs['view']);
    expect(svg.select(".content").selectAll("rect").empty()).toBeFalsy();

    state.current_tree = null;
    await Vue.nextTick();
    svg = d3.select(wrapper.vm.$refs['view']);
    expect(svg.select(".content").selectAll("rect").empty()).toBeTruthy();
  });

  test("Progressive tooltip", async () => {
    state.current_tree = generateMainViewTree();
    const wrapper = wrapperFactory();

    let svg     = d3.select(wrapper.vm.$refs['view']);
    let nodes   = svg.select(".content").selectAll("rect");
    let tooltip = wrapper.vm.$refs['tooltip'];

    let check_rec = async function(node) {
      let d3Node = nodes.filter((d) => d.data.id == node.id);

      //Test mouseover
      d3Node.node().dispatchEvent(new Event('mouseover'));
      let textContent = tooltip.textContent;
      expect(textContent).toContain(node.name);
      expect(parseFloat(tooltip.style.opacity)).toBeGreaterThan(0.1);
      expect(tooltip.style.display).not.toBe("none");

      //Set the node data
      node.count *= 2;
      state.current_tree = {...state.current_tree};
      node = treeUtils.searchNodeByID(state.current_tree, node.id);
      await Vue.nextTick();
      expect(textContent).not.toBe(tooltip.textContent);
      expect(tooltip.textContent).toContain(node.name);

      d3Node.node().dispatchEvent(new Event('mouseout'));
      expect(parseFloat(tooltip.style.opacity) == 0.0 || tooltip.style.display == "none").toBeTruthy();

      for(let i = 0; i < node.children.length; i++)
        await check_rec(node.children[i]);
    };

    for(let i = 0; i < state.current_tree.children.length; i++)
      await check_rec(state.current_tree.children[i]);
  });
});

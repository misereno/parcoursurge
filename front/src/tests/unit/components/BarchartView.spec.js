import { shallowMount } from '@vue/test-utils';
import Vue from 'vue';
import * as d3 from 'd3';
import BarchartView from '@/components/BarchartView.vue'
import { parseTransform } from '../utils.js';

function wrapperFactory() {
    const wrapper = shallowMount(BarchartView, {
                                   propsData: {
                                       height: 300,
                                       width: 300,
                                       padding: [0, 0, 0, 0],
                                       chartdata: [
                                           {name: "Disease1", value: 0},
                                           {name: "Disease2", value: 10},
                                           {name: "Disease3", value: 20},
                                       ],
                                       domain: ["Disease3", "Disease2", "Disease1"], //Test a reversed order
                                       colordomain: {Disease1: "#ff0000", Disease2: "#00ff00", Disease4: "#0000ff"},
                                       nbValues: 0
                                   }
                                 }
    );
    return wrapper;
}

describe("BarchartView", () => {
    test("Axis nbValues=0", () => {
        const wrapper = wrapperFactory();

        let svg    = d3.select(wrapper.vm.$refs.hist);
        let xAxis  = svg.select(".axis--x");
        let yAxis  = svg.select(".axis--y");
        let xTicks = xAxis.selectAll(".tick");
        let yTicks = yAxis.selectAll(".tick");

        //Test that we have the correct number of ticks:
        expect(xTicks.size()).toBe(wrapper.vm.$props.domain.length);

        //Test the size (using translate as nodejs does not have any layout system)
        //We cannot test the X axis as it is not based on ticks...
        xTicks.each(function(tick, ind) {
            expect(d3.select(this).select('text').text()).toBe(wrapper.vm.$props.domain[ind]);
        });

        let minSize = NaN;
        let maxSize = minSize;
        yTicks.each(function(_tick, _ind) {
            //Compute pos
            let pos = parseFloat(parseTransform(d3.select(this).attr("transform")).translate[1]);
            if(isNaN(minSize))
                minSize = maxSize = pos;
            else if(minSize > pos)
                minSize = pos;
            else if(maxSize < pos)
                maxSize = pos;

            //Check that there is no percentage in the list. The tick should be parseable in Float
            expect(parseFloat(d3.select(this).select('text').text())).not.toBeNaN();
        });
        expect(maxSize-minSize).toBe(wrapper.vm.$props.height); //This is a weak test: The ticks may not go to the limit (one tick may be missing)
    });

    test("Axis nbValues!=0", async () => {
        const wrapper = wrapperFactory();
        wrapper.setProps({nbValues: 100});

        await Vue.nextTick(); //Wait for the change to apply

        let svg    = d3.select(wrapper.vm.$refs.hist);
        let yAxis  = svg.select(".axis--y");
        let yTicks = yAxis.selectAll(".tick");

        //Test that we have percentage symbol
        yTicks.each(function(tick, _ind) {
            expect(d3.select(this).select('text').text().slice(-1)).toBe('%');
            expect(parseFloat(d3.select(this).select('text').text().slice(0, -1))).not.toBeNaN();
            expect(parseFloat(d3.select(this).select('text').text().slice(0, -1))).toBeCloseTo(tick*100);
        });
    });

    test("nb bars", () => {
        const wrapper = wrapperFactory();

        let main = d3.select(wrapper.vm.$refs.main);
        expect(main.selectAll("rect").size()).toBe(wrapper.vm.$props.chartdata.length);
    });

    test("Size bars", () => {
        const wrapper = wrapperFactory();

        let main = d3.select(wrapper.vm.$refs.main);
        main.selectAll("rect").each(function(data, ind) {
            let relHeight = data.value/(d3.max(wrapper.vm.$props.chartdata.map((x) => x.value)));
            expect(data).toBe(wrapper.vm.$props.chartdata[ind]);
            expect(parseFloat(d3.select(this).attr('height'))).toBeCloseTo(relHeight*wrapper.vm.$props.height);
        });
    });

    test("Color bars", () => {
        const wrapper = wrapperFactory();
        let colors = {...wrapper.vm.$props.colordomain,  Disease3: "#000000"}; //We add Disease3 which was not defined in colordomain. Default color: black (#000000)

        let main = d3.select(wrapper.vm.$refs.main);
        main.selectAll("rect").each(function(data, _ind) {
            expect(d3.select(this).attr('fill')).toBe(colors[data.name]);
        });
    });
});

import { shallowMount } from '@vue/test-utils';
import * as d3 from 'd3';
import HistogramView from '@/components/HistogramView.vue';
import { parseTransform } from '../utils.js';

function wrapperFactory() {
    const wrapper = shallowMount(HistogramView, {
                                   propsData: {
                                       height: 300, 
                                       width: 300, 
                                       padding: [0, 0, 0, 0],
                                       color: "blue",
                                       chartdata: {
                                           bins: [4, 2, 3, 6],
                                           range: [0, 15]
                                       }
                                   }
                                 });

    return wrapper;
}

describe("HistogramView", () => {
    test("Axis", () => {
        const wrapper = wrapperFactory();

        let svg    = d3.select(wrapper.vm.$refs.hist);
        let xAxis  = svg.select(".axis--x");
        let yAxis  = svg.select(".axis--y");
        let xTicks = xAxis.selectAll(".tick");
        let yTicks = yAxis.selectAll(".tick");

        //Test that we have the correct number of ticks:
        expect(xTicks.size()).toBe(wrapper.vm.$props.chartdata.bins.length+1);

        //Test the size (using translate as nodejs does not have any layout system)
        let minSize = NaN;
        let maxSize = minSize;
        xTicks.each(function(_tick, _ind) {
            let pos = parseInt(parseTransform(d3.select(this).attr("transform")).translate[0]);
            if(isNaN(minSize))
                minSize = maxSize = pos;
            else if(minSize > pos)
                minSize = pos;
            else if(maxSize < pos)
                maxSize = pos;
        });
        expect(maxSize-minSize).toBe(wrapper.vm.$props.width);

        minSize = maxSize = NaN;
        yTicks.each(function(_tick, _ind) {
            let pos = parseInt(parseTransform(d3.select(this).attr("transform")).translate[1]);
            if(isNaN(minSize))
                minSize = maxSize = pos;
            else if(minSize > pos)
                minSize = pos;
            else if(maxSize < pos)
                maxSize = pos;
        });
        expect(maxSize-minSize).toBe(wrapper.vm.$props.height);
    });

    test("Nb bars", () => {
        const wrapper = wrapperFactory();

        let main = d3.select(wrapper.vm.$refs.main);
        expect(main.selectAll(".dataRect").size()).toBe(wrapper.vm.$props.chartdata.bins.length);
    });

    test("Size bars", () => {
        const wrapper = wrapperFactory();

        let main = d3.select(wrapper.vm.$refs.main);
        main.selectAll(".dataRect").each(function(data, _ind) {
            let relHeight = data/(d3.max(wrapper.vm.$props.chartdata.bins));
            expect(parseFloat(d3.select(this).attr('height'))).toBeCloseTo(wrapper.vm.$props.height*relHeight);
            expect(parseFloat(d3.select(this).attr('width'))).toBeCloseTo(wrapper.vm.$props.width/wrapper.vm.$props.chartdata.bins.length);
        });
    });
});

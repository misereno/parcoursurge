import { getters, mutations } from "@/plugins/vuex";
import { History, HistoryNode } from '@/History';
import { TabView, ViewType, ComparisonMode, TreeToDisplay } from '@/Config';
import { generateMainViewTree, generateSequenceViewTree } from "../utils";

describe("Vuex Mutations", () => {
  test("set_history", () => {
    const state = {
      history: new History(new HistoryNode("root", {}, "root")),
      history_listener: {
        onUndo:     jest.fn(),
        onRedo:     jest.fn(),
        onJumpNode: jest.fn(),
      }
    };

    //Create a default history root --> child2
    let history    = new History(new HistoryNode("root", {}, "root"));
    let childNode1 = new HistoryNode("type2", {id: 1}, "child1");
    history.insertNode(childNode1, true);
    let childNode2 = new HistoryNode("type3", {id: 2}, "child2");
    history.insertNode(childNode2, true);

    //Set the state history and check than onJumpNode is called with child2 argument
    mutations.set_history(state, history);

    expect(state.history_listener.onJumpNode).toHaveBeenCalledTimes(1);
    expect(state.history_listener.onJumpNode).toHaveBeenLastCalledWith(childNode2, history.tree);
  });

  test("insert_history_node", () => {
    const history = new History(new HistoryNode("root", {}, "root"));
    const state = {
      history:          history,
      comparison_mode : ComparisonMode.NONE_COMPARISON_MODE,
      comparison_nodes: [null, null],
    };

    //Insert a new node while no comparison is performed
    let newNode = new HistoryNode("type2", {id: 1}, "child1");
    mutations.insert_history_node(state, newNode); 
    expect(getters.history(state).curNode).toBe(newNode);
    expect(getters.history(state).tree.children[0]).toBe(newNode);

    //Insert a new node while in SOURCE comparison
    state.comparison_mode = ComparisonMode.SOURCE_COMPARISON_MODE;
    state.comparison_nodes[ComparisonMode.SOURCE_COMPARISON_MODE] = history.tree;
    history.jumpTo(history.tree);
    let newNode2 = new HistoryNode("type3", {id: 2}, "child2");
    mutations.insert_history_node(state, newNode2); 
    expect(getters.history(state).curNode).toBe(newNode2);
    expect(getters.comparison_nodes(state)[ComparisonMode.SOURCE_COMPARISON_MODE]).toBe(newNode2);
    expect(getters.history(state).tree.children[1]).toBe(newNode2);

    //Insert a new node while in TARGET comparison
    state.comparison_mode = ComparisonMode.TARGET_COMPARISON_MODE;
    state.comparison_nodes[ComparisonMode.TARGET_COMPARISON_MODE] = history.curNode;
    let newNode3 = new HistoryNode("type4", {id: 3}, "child3");
    mutations.insert_history_node(state, newNode3); 
    expect(getters.history(state).curNode).toBe(newNode3);
    expect(getters.comparison_nodes(state)[ComparisonMode.TARGET_COMPARISON_MODE]).toBe(newNode3);
    expect(getters.history(state).tree.children[1]).toBe(newNode2);
    expect(getters.history(state).tree.children[1].children[0]).toBe(newNode3);
  });

  test("set_tree", () => {
    const state = {
      view_type: ViewType.SEQUENCE_VIEW_TYPE,
      full_tree: null,
      align_sequence: [],
      align_sequence_by: 0,
      attributes: {prune_threshold: {min: 0, max: 1000}},
    };

    const store = {state, mutations: {
      ...mutations, 
        commit: (t, value) => {
          expect(t).toBe("set_view_type");
          expect(value == ViewType.MAIN_VIEW_TYPE || value == ViewType.SEQUENCE_VIEW_TYPE).toBeTruthy();
          state.view_type = value;
        }
      }
    };
    //Test for main view trees
    let tree = generateMainViewTree();
    let count = tree.count;
    store.mutations.set_tree(state, tree);
    expect(state.full_tree).toMatchObject(tree);
    expect(getters.view_type(state)).toBe(ViewType.MAIN_VIEW_TYPE);
    expect(getters.total(state)).toBe(count);
    expect(getters.expected_total(state)).toBe(2*count);
    expect(getters.filtered_total(state)).toBe(5);
    expect(getters.attributes(state).prune_threshold.max).toBeLessThan(count);

    //Test for dual sequence trees
    tree = generateSequenceViewTree();
    tree.align_sequence = [3, 1];
    store.mutations.set_tree(state, tree);
    expect(getters.view_type(state)).toBe(ViewType.SEQUENCE_VIEW_TYPE);
    expect(getters.align_sequence(state)).toStrictEqual(["phyto", "surgery"]);
    expect(getters.align_sequence_by(state)).toBe(0);
  });

  test("set_view_type", () => {
    const state = {
      view_type: ViewType.MAIN_VIEW_TYPE
    };
    const store = {state, 
      mutations: {
        ...mutations, 
        commit: jest.fn()
      }
    };

    store.mutations.set_view_type(state, ViewType.SEQUENCE_VIEW_TYPE);
    expect(store.mutations.commit).toHaveBeenCalledTimes(1);
    expect(store.mutations.commit).toHaveBeenLastCalledWith("set_display");
    expect(getters.view_type(store.state)).toBe(ViewType.SEQUENCE_VIEW_TYPE);

    //Try invalid value
    store.mutations.set_view_type(state, ViewType.END_VIEW_TYPE);
    expect(store.mutations.commit).toHaveBeenCalledTimes(1);
    expect(getters.view_type(store.state)).toBe(ViewType.SEQUENCE_VIEW_TYPE);
  });

  test("set_sequence_view_curtree_to_display", () => {
    const state = {
      sequence_view_curtree_to_display : TreeToDisplay.BEFORE
    };

    mutations.set_sequence_view_curtree_to_display(state, TreeToDisplay.AFTER);
    expect(getters.sequence_view_curtree_to_display(state)).toBe(TreeToDisplay.AFTER);
  });

  test("set_current_sequence", () => {
    const state = {
      current_sequence: []
    };

    mutations.set_current_sequence(state, ["phyto"]);
    expect(getters.current_sequence(state)).toStrictEqual(["phyto"]);
  });

  test("set_current_tree", () => {
    const state = {
      view_type: ViewType.SEQUENCE_VIEW_TYPE,
      full_tree: null,
      align_sequence: [],
      align_sequence_by: 0,
      attributes: {prune_threshold: {min: 0, max: 1000}},
    };

    const store = {state, mutations: {
      ...mutations, 
        commit: (t, value) => {
          expect(t).toBe("set_view_type");
          expect(value == ViewType.MAIN_VIEW_TYPE || value == ViewType.SEQUENCE_VIEW_TYPE).toBeTruthy();
          state.view_type = value;
        }
      }
    };

    //------------------------------------------------------------------------------
    //------------Functions to check, recursively, some computed values-------------
    //------------------------------------------------------------------------------
    function checkSort_rec(node) {
      if(node == null || node.children == undefined || node.children.length <= 1)
        return;
      for(let i = 0; i < node.children.length-1; i++) {
        expect(node.children[i].count).toBeLessThanOrEqual(node.children[i+1].count);
        checkSort_rec(node.children[i]);
      }
      checkSort_rec(node.children[node.children.length-1]);
    }

    function checkStart_rec(node, curStart) {
      expect(node.count_start).toBe(curStart);
      if(node.children == null)
        return;
      for(let i = 0; i < node.children.length; i++) {
        checkStart_rec(node.children[i], curStart);
        curStart += node.children[i].count;
      }
    }

    function checkMaxDuration_rec(node) {
      if(node.children == null || node.children.length == 0) {
        expect(node.max_duration).toBe(node.duration);
        return node.duration;
      }

      let maxDuration = node.duration + Math.max(...node.children.map((child) => checkMaxDuration_rec(child)));
      expect(node.max_duration).toBe(maxDuration);
      node.children.forEach((child) => checkMaxDuration_rec(child));

      return maxDuration;
    }

    //Empty tree
    let tree = null;
    store.mutations.set_tree(store.state, tree);
    store.mutations.set_current_tree(store.state);
    expect(getters.tree(store.state)).toBeNull();

    //Main view
    tree = generateMainViewTree();
    store.mutations.set_tree(store.state, tree);
    store.mutations.set_current_tree(store.state);
    checkSort_rec(getters.tree(store.state));
    checkStart_rec(getters.tree(store.state), 0);
    checkMaxDuration_rec(getters.tree(store.state));

    //Sequential view
    tree = generateSequenceViewTree();
    tree.reset = true;
    store.mutations.set_tree(store.state, tree);
    store.mutations.set_current_tree(store.state);
    //nodes just after a "dual_sequence" type should not be sorted!
    checkSort_rec(getters.tree(store.state).children[0].children[0]);
    checkSort_rec(getters.tree(store.state).children[0].children[1]);
    checkStart_rec(getters.tree(store.state), 0);
    checkMaxDuration_rec(getters.tree(store.state));

    expect(getters.tree(store.state).children[0].children[0].name).toBe("after");
    expect(getters.tree(store.state).children[0].children[1].name).toBe("before");
  });

  test("send_message", () => {
    const state = {
      unique_event_id: 0,
      ws: {send: jest.fn()}
    };

    const object = {type: 'foo', value: 'bar'};
    mutations.send_message(state, {data: object});
    expect(state.ws.send).toHaveBeenCalledTimes(1);
    expect(JSON.parse(state.ws.send.mock.calls[0][0])).toStrictEqual({...object, event_id: 0});
    expect(state.unique_event_id).toBe(1);
  });

  test("set_tab", () => {
    const state = {
      tab: TabView.OVERVIEW_TAB
    };

    let lastValue = state.tab;
    for(const value of Object.values(TabView)) {
      mutations.set_tab(state, value);
      expect(getters.tab(state)).toBe(value);
      lastValue = value;
    }

    mutations.set_tab(state, "foo");
    expect(getters.tab(state)).toBe(lastValue);
  });

  test("set_distribution", () => {
    const state = {
      distribution: null
    };

    const object = {
      age: {bins: [32, 42]},
      duration: {bins: [45, 46]},
      disease: [{name: "Hypertension", value: 42}],
      nbValues: 74
    };
    mutations.set_distribution(state, object);
    expect(getters.distribution_age(state)).toStrictEqual(object.age);
    expect(getters.distribution_duration(state)).toStrictEqual(object.duration);
    expect(getters.distribution_disease(state)).toStrictEqual(object.disease);
    expect(getters.distribution_nbValues(state)).toBe(object.nbValues);
  });

  test("set_ws", () => {
    const state = {
      ws: null
    };

    const object = {type: 'foo', value: 'bar'};
    mutations.set_ws(state, object);
    expect(state.ws).toStrictEqual(object);
  });

  test("set_type_hierarchy", () => {
    const state = {
      type_hierarchy: null,
    };

    const root = {type_combination: 0, name: "root", children: []};
    mutations.set_type_hierarchy(state, root);

    expect(state.type_hierarchy).toStrictEqual(root);
  });
});

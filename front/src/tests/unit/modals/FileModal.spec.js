import { mount, createLocalVue } from '@vue/test-utils';

import Vuetify from 'vuetify';
import Vue from 'vue';

import FileModal from '@/modals/FileModal';

Vue.use(Vuetify);

const localVue = createLocalVue();
const vuetify  = new Vuetify();

function wrapperFactory() {
  const wrapper = mount(FileModal, {
                          localVue, vuetify,
                          propsData: {
                            showModal: true,
                            inputMsg: "SelectFile",
                          }
                       });
  return wrapper;
}

describe('FileModal.vue', () => {
  let wrapper = null;

  afterEach(() => {
    if(wrapper != null)
      wrapper.destroy();
    wrapper = null;
  });

  test("Title", () => {
    wrapper = wrapperFactory();
    expect(wrapper.html()).toContain("SelectFile");
  });

  test("SelectFile", async () => {
    wrapper = wrapperFactory();
    let input   = wrapper.find('input[type="file"]');
    input.element.value = "";
    await input.trigger("change");

    expect(wrapper.emitted().done).toBeTruthy(); //Cannot check the file, though...
    expect(wrapper.emitted().close).toBeUndefined();
  });

  test("Keydown", async () => {
    wrapper = wrapperFactory();
    var event = new KeyboardEvent('keydown', {'keyCode': 27}); //escape
    document.dispatchEvent(event);
    expect(wrapper.emitted().close).toBeTruthy();
    expect(wrapper.emitted().done).toBeUndefined();
  });
});

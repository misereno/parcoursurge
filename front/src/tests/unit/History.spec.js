import { History, HistoryNode } from '@/History';

function wrapperFactory() {
  let history = new History(new HistoryNode("root", {}, "root"));
  return history;
}

describe("History", () => {
  test("init", () => {
    let history = wrapperFactory();    
    expect(history.tree).toBe(history.curNode);

    //Check disableSaving
    history.disableSaving = true;
    history.insertNode(new HistoryNode("", {}, ""));
    
    expect(history.tree).toBe(history.curNode);
    expect(history.curNode.children.length).toBe(0);
  });

  test("listener", () => {
    let history = wrapperFactory();
    let listener = {
      onUndo     : jest.fn(),
      onRedo     : jest.fn(),
      onJumpNode : jest.fn(),
    };
    expect(history.addListener(listener)).toBeTruthy();
    expect(history.addListener(listener)).toBeFalsy();

    //Insert a child before playing the history
    let childNode = new HistoryNode("type", {}, "child1");
    history.insertNode(childNode, true);
    expect(history.curNode).toBe(childNode);

    //Jump to curNode
    expect(history.jumpTo(history.curNode)).toBeFalsy();
    expect(listener.onJumpNode).toHaveBeenCalledTimes(0); //No need to jump to curNode
    
    //Jump to "root"
    expect(history.jumpTo(history.tree)).toBeTruthy();
    expect(listener.onJumpNode).toHaveBeenCalledTimes(1); //Jump once
    expect(history.curNode).toBe(history.tree);
    
    //Undo root
    expect(history.undo()).toBeFalsy(); //Cannot undo root
    expect(listener.onUndo).toHaveBeenCalledTimes(0);

    //Redo empty
    expect(history.redo()).toBeFalsy(); //Cannot redo a state that was not undone
    expect(listener.onRedo).toHaveBeenCalledTimes(0);

    //Undo childNode
    history.jumpTo(childNode);
    expect(history.undo()).toBeTruthy();
    expect(listener.onUndo).toHaveBeenCalledTimes(1);

    //Redo childNode
    expect(history.redo()).toBeTruthy(); //Cannot redo a state that was not undone
    expect(listener.onRedo).toHaveBeenCalledTimes(1);

    listener.onRedo = jest.fn();
    expect(history.redo()).toBeFalsy(); //Cannot redo more than once as we have only one event (in addition to root)
    expect(listener.onRedo).toHaveBeenCalledTimes(0);
    expect(history.curNode).toBe(childNode);

    //Jump to non-added node
    listener.onJumpNode = jest.fn();
    let uselessNode = new HistoryNode("", {}, "");
    history.jumpTo(uselessNode);
    expect(listener.onJumpNode).toHaveBeenCalledTimes(0);

    //Remove listener
    expect(history.removeListener(listener)).toBeTruthy();
    expect(history.removeListener(listener)).toBeFalsy();
  });

  test("replay history", () => {
    let history = wrapperFactory();

    let clbk = jest.fn();
    history.replayHistory(clbk, history.curNode);
    expect(clbk).toHaveBeenCalledTimes(2); //called twice: Once to reinit the state, once to set "root"
    
    //Redo the same, as "null" will be converted to history.curNode
    clbk = jest.fn();
    history.replayHistory(clbk, null);
    expect(clbk).toHaveBeenCalledTimes(2); //called twice: Once to reinit the state, once to set "root"
    
    //Insert a child before replaying the history from curNode to childNode
    let childNode = new HistoryNode("type", {}, "child1");
    history.insertNode(childNode, false);
    expect(history.curNode).toBe(history.tree);

    clbk = jest.fn();
    history.replayHistory(clbk, childNode);
    expect(clbk).toHaveBeenCalledTimes(1); //called once: only childNode, as childNode is a child of root
    
    //Insert an absolute child before replaying the history from curNode to childNode
    let absoluteChildNode = new HistoryNode("absoluteType", {}, "child2");
    history.insertNode(absoluteChildNode, true);
    history.setAbsoluteTypes(["absoluteType"]);
    expect(history.curNode).toBe(absoluteChildNode);

    //Reset the history to curNode == root
    history.jumpTo(history.tree);
    expect(history.curNode).toBe(history.tree);

    //Then replay the history
    clbk = jest.fn();
    history.replayHistory(clbk, absoluteChildNode);
    expect(clbk).toHaveBeenCalledTimes(1); //called once: while we had: tree (curNode) --> childNode 
    //                                                                                 --> absoluteChildNode, absoluteChildNode contains all the data needed. No need to replay the whole history
    history.jumpTo(absoluteChildNode);
    expect(history.curNode).toBe(absoluteChildNode);
    
    //Insert more children
    let childNode3 = new HistoryNode("type", {}, "child3");
    history.insertNode(childNode3, true);
    expect(history.curNode).toBe(childNode3);

    let childNode4 = new HistoryNode("type", {}, "child4");
    history.insertNode(childNode4, true);
    expect(history.curNode).toBe(childNode4);

    //Reset the history to curNode == root
    history.jumpTo(history.tree);
    expect(history.curNode).toBe(history.tree);

    //Make a jump from root to childNpde4 (which is a child of root)
    clbk = jest.fn();
    history.replayHistory(clbk, childNode4);
    expect(clbk).toHaveBeenCalledTimes(3); //called three times: while we had: tree (curNode) --> childNode --> absoluteChildNode --> childNode3 --> childNode4, absoluteChildNode contains all the data needed. No need to replay the whole history before absoluteChildNode
    expect(history.curNode).toBe(history.tree); //curnode should not have been modified
    
    //Make a jump from childNode4 to childNode3 (which IS NOT a child of childNode4)
    history.jumpTo(childNode4);
    clbk = jest.fn();
    history.replayHistory(clbk, childNode3);
    expect(clbk).toHaveBeenCalledTimes(2); //called twice times: while we had: tree --> childNode --> absoluteChildNode --> childNode3 --> childNode4 (curNode), absoluteChildNode contains all the data needed. No need to replay the whole history before absoluteChildNode

    let listener = {
      onUndo     : jest.fn(),
      onRedo     : jest.fn(),
      onJumpNode(newNode, oldNode) {
        expect(history.curNode).toBe(history.tree); //curnode should not have been modified
        expect(newNode).toBe(childNode4);
        expect(oldNode).toBe(history.curNode);

        clbk = jest.fn();
        history.replayHistory(clbk, newNode);
        expect(clbk).toHaveBeenCalledTimes(3); //called three times just as before
        
        clbk = (_node) => {
          expect(history.disableSaving).toBeTruthy();
        };
        expect(history.disableSaving).toBeFalsy();
        history.replayHistory(clbk, newNode);
      }
    };
    history.addListener(listener);
    history.jumpTo(childNode4);
    expect(history.curNode).toBe(childNode4);
  });

  test("Import/Export", () => {
    let history = wrapperFactory();
    let childNode1 = new HistoryNode("type2", {id: 1}, "child1");
    history.insertNode(childNode1, false);

    let childNode2 = new HistoryNode("type2", {id: 2}, "child2");
    history.insertNode(childNode2, true);

    let childNode3 = new HistoryNode("type3", {id: 3}, "child3");
    history.insertNode(childNode3, true);

    //tree state: root --> child1
    //                 --> child2 --> child3 (curNode);


    //Check that we export a correct JSON object
    let jsonExport = history.toString();
    let jsonObject = JSON.parse(jsonExport);
    expect(jsonObject).toBeDefined();

    //Import that json to another history
    let importedHistory = History.importFromJSON(jsonExport);
    expect(importedHistory).toBeDefined();

    //Check that curNode is respected
    expect(importedHistory.curNode.label).toBe("child3");

    //Check the topology
    let topology_rec = (curNodeOriginal, curNodeImported) => {
      if(curNodeOriginal == null || curNodeImported == null) {
        expect(curNodeOriginal).toBe(curNodeImported);
        return;
      }

      //Check the data
      expect(curNodeOriginal.label).toBe(curNodeImported.label);
      expect(curNodeOriginal.type).toBe(curNodeImported.type);
      expect(curNodeOriginal.timestamp).toBe(curNodeImported.timestamp);
      expect(curNodeOriginal.ephemeral).toBe(curNodeImported.ephemeral);
      expect(curNodeOriginal.bookmarked).toBe(curNodeImported.bookmarked);
      expect(curNodeOriginal.data.id).toBe(curNodeImported.data.id);
      expect(curNodeOriginal.children.length).toBe(curNodeImported.children.length);

      for(let i = 0; i < curNodeOriginal.children.length && i < curNodeImported.children.length; i++)
        topology_rec(curNodeOriginal.children[i], curNodeImported.children[i]);
    };
    topology_rec(history.tree, importedHistory.tree);

  });
});

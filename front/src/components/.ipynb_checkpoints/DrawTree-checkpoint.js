import * as d3 from "d3"
import * as tooltip from "./Tooltip"
import { TabView, RootEntries } from '../Config'

let drawtree = function(vue, refs, treedata, curtree, innerWidth, isright, svgname, zoomIn, tooltipPrecisionStr="")
{

  //The div which will contain tooltip information
  const tooltipDiv = d3.select(refs.tooltip);

  let svg = d3.select(refs[svgname]) 
  if(curtree == null){
    svg.select(`.content`).selectAll("rect").remove();
    return;
  }

  //Update the tooltip in a progressive environment
  if(vue.mouseover_nodeid != null) {
    let finddata_rec = (node) => {
      if(node.id == vue.mouseover_nodeid)
        return node;
      for(let child of node.children) {
        let res = finddata_rec(child);
        if(res != null)
          return res;
      }
      return null;
    };

    var mouseover_data = finddata_rec(curtree);
    if(mouseover_data != null)
      tooltipDiv.html(`${mouseover_data.name}: ${mouseover_data.count.toLocaleString()} (${(mouseover_data.count/curtree.count*100).toFixed(2)}%) ${tooltipPrecisionStr}`);
  }
    //console.log("vue.display.main.innerHeight:", vue.display.main.innerHeight);
    //console.log("curtree.count:", curtree.count);
    
  let unitsheight=curtree.count;
    //console.log("unitsheight:", unitsheight);


  let unitheight = unitsheight == 0 ? vue.display.main.innerHeight : vue.display.main.innerHeight / unitsheight;
    
    //console.log("unitheight:", unitheight);

  let unitswidth = curtree.max_duration;
  let unitwidth = innerWidth / (unitswidth)

  var root = d3.partition()
    .size([vue.display.main.innerHeight, innerWidth])
    //.size([1000, innerWidth])
    .padding(1)(d3.hierarchy(curtree)
                .sum(() => 1)
                .sort())

  //const svg = d3.select(".view")
  
  const t = svg.transition()
    .duration(400);

  let computewidth = (d) => (RootEntries.includes(d.data.name) ? 0 : (d.data.duration && d.data.duration > 0) ? unitwidth * d.data.duration : 5);
  let computetranslate = (d) =>
  {
  let posX = Math.max(0, (d.parent && d.parent.data.cum_duration ? unitwidth * (d.parent.data.cum_duration - (curtree.cum_duration - curtree.duration)) : 0));
  let posY = Math.max(0, (d.data.count_start - curtree.count_start) * unitheight);

  if(!isright)
      posX = Math.max(0, innerWidth - computewidth(d) - posX);

  return `translate(${posX},${posY})`;
  }

  let onClick = (curRect, d) => 
  {
    vue.get_distribution({ node_id: d.data.id, action: 'distribution', color: vue.color_map[d.data.name] });
    vue.set_tab(TabView.DETAIL_TAB);
    vue.updatenodeprefix(treedata, d.data);
    let selected = svg.select(".content").select(".selectedNode");
    if(!selected.empty())
    {
      selected.attr("class", "");
      if(selected.node() == curRect)
        return;
    }
    d3.select(curRect).attr("class", `selectedNode`); 
  };
  
  vue.rects = svg.select(`.content`).selectAll("rect").data(root.descendants().reverse(), (d) => d.data.id)
      .join(
        enter => enter.append("rect")
          .attr("id",        d => d.data.id )
          .attr("width",     d => computewidth(d))
          .attr("height",    d => {
          let height = Math.max(2, d.data.count ? d.data.count * unitheight : 0);
        //console.log("Rectangle height:", height, "for node:", d.data.name);
        //console.log("top", computewidth(d));
          return height;
        })
          .attr("transform", d => computetranslate(d))
          .attr("stroke",  "#fff")
          .attr("fill",  "#fff")
          .on("mouseover", function(event, d) {
            vue.mouseover_nodeid = d.data.id;
            tooltip.mouseover(tooltipDiv, event, `${d.data.name}: ${d.data.count.toLocaleString()} (${(d.data.count/curtree.count*100).toFixed(2)}%) ${tooltipPrecisionStr}`);
          })
          .on("mouseout", function() {
            vue.mouseover_nodeid = null;
            tooltip.mouseout(tooltipDiv);
          })
          .on("click",     function(_e, d) { 
            onClick(this, d);
          })
          .on("dblclick", function(e, d) {
            zoomIn(d.data);
            e.stopPropagation();
          })
          .call(enter => enter.transition(t)
            .attr("fill", d => vue.color_map[d.data.name] || vue.color_map.default)
          ),

        update => update.call(update => update.transition(t)
            .attr("width", d => computewidth(d))
            .attr("height", d => Math.max(2, d.data.count ? d.data.count * unitheight : 0))
            .attr("transform", d => computetranslate(d))
            .attr("fill", d => vue.color_map[d.data.name] || vue.color_map.default)
          )
          .on("mouseover", function(event, d) {
            vue.mouseover_nodeid = d.data.id;
            tooltip.mouseover(tooltipDiv, event, `${d.data.name}: ${d.data.count.toLocaleString()} (${(d.data.count/curtree.count*100).toFixed(2)}%) ${tooltipPrecisionStr}`);
          })
          .on("click",     function(_e, d) { 
            vue.mouseover_nodeid = null;
            onClick(this, d);
          })
          .on("dblclick", function(e, d) {
            zoomIn(d.data);
            e.stopPropagation();
          }),

        exit => exit.call(exit => exit.remove()
          )
      )

  let x = null
  
  if(isright)
    x = d3.scaleLinear().domain([curtree.cum_duration-curtree.duration, unitswidth+curtree.cum_duration-curtree.duration]).range([0, innerWidth]);
  else
    x = d3.scaleLinear().domain([unitswidth+curtree.cum_duration-curtree.duration, curtree.cum_duration-curtree.duration]).range([0, innerWidth])
    
var scaleInMinutes = d3.scaleLinear()
    .domain(x.domain().map(function(d) { return d / 60; })) 
    .range(x.range());
    
  let xAxis = d3.axisBottom(scaleInMinutes)
  // transition to resize when load value > 3
  svg.select(`.axis--x`).transition(d3.easeLinear).duration(750).call(xAxis)
}

export { drawtree }



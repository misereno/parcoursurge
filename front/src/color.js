/** \brief The colormap to use for the different type of events. */
let color_map = {
 /*"death"          : "#ffeda0",
 "Hall_d_entree"  : "#84B1D8",
 "IOA"            : "#EE8373",
 "Boxes"          : "#B9DC7C",
 "Zone_1"         : "#D9D9D9",
 "Ambu"           : "#F4B862",
 "ZSA"            : "#D9302C",
 "Zone_2"         : "#999999",
 "Dechocage"      : "#B582BB",
 "unknown0"       : "#A0562F",
 "interruption"   : "#555555",
 "default"        : "lightgray",*/
 "WAITING_AREA"     : "#ffeda0",
 "STANDARD_CUBICLE" : "#84B1D8",
 "TECHNICAL_CUBICLE": "#EE8373",
 "TRIAGE_ROOM"      : "#B9DC7C",
 "SHOCK_ROOM"       : "#D9302C",
 "interruption"     : "#555555",
 "default"          : "lightgray",
 "UHCD"             : "#F4B862",
 "ENTRANCE"         : "#999999", 
 "EXIT"             : "#B582BB",
    
    
};

/** \brief In addition to color_map, this object lists, for all type of event, whether the background (color_map) is a dark color */
let is_color_dark = {
 "WAITING_AREA"     : false,
 "STANDARD_CUBICLE" : false,
 "TECHNICAL_CUBICLE": false,
 "TRIAGE_ROOM"      : false,
 "SHOCK_ROOM"       : false,
 "UHCD"             : false,
 "ENTRANCE"         : false,
 "EXIT"             : false,
 "interruption"     : false,
 "default"          : false,

};

export { color_map, is_color_dark };

import sys
import os
import csv
import random
from itertools import *

def printDocAndExit():
    print("Run ./import_from_simulation.py [originalData] [--output CSVOutput] [-h] [--help]")
    print("originData: the original simulation data as a CSV file. Default: data.csv")
    print("--output: output path to save the new suitable and ready to be preparsed data in a CSV format. Default: output.csv")
    print("-h --help: Print this documentation and exit.")

    sys.exit(-1)

class Patient:
    def __init__(self, patientID, age):
        self.patientID = patientID
        self.age       = age
        
        self._utc         = list()
        self._categories  = list()
        self._subservices = list()

    def __iter__(self):
        return zip(self._utc, self._categories, self._subservices)

    def addEvent(self, utc, category, subservice):
        self._utc.append(utc)
        self._categories.append(category)
        self._subservices.append(subservice)

if __name__ == '__main__':
    argv       = sys.argv[1:]
    dataPath   = "data.csv"
    files      = []
    outputPath = os.getcwd() + "/output.csv"

    i = 0
    while i < len(argv):
        #Read input file names (beginning of the command)
        arg = argv[i]
        if i == 0 and not (arg.startswith("--") or arg.startswith("-")):
            dataPath = arg
            i+=1
        else:
            while i < len(argv):
                arg = argv[i]

                if arg == "-h" or arg == "--help":
                    printDocAndExit()

                elif arg == "--output":
                    if i < len(argv)-1:
                        outputPath = argv[i+1]
                        i+=1
                    else:
                        print("Missing CSV file path value to '--output' parameter")
                        sys.exit(-1)
                else:
                    print("Unknown parameter {}".format(arg))
                    printDocAndExit()
                i+=1

    assert os.path.isfile(dataPath)

    #Save all the data of every patient because they need to be sorted
    patients = dict()

    #Read row by row the original CSV file and store, per patient, their data
    #We assume that the time column, in the original CSV file, is already ordered
    with open(dataPath, "r") as f:
        reader = csv.DictReader(f, delimiter=',')
        for row in reader:
            patientID = int(row['patient'].split('#')[1].strip())
            curPatient = None
            if patientID in patients:
                curPatient = patients[patientID]
            else:
                curPatient = Patient(patientID, random.randint(18, 99))
                patients[patientID] = curPatient

            category = row['trans_zone']
            if category == '':
                continue

            #Save the time and the current patient position
            curPatient.addEvent(int(float(row['t'])*60.0), category, row['health_pro'])

    #Now that everything is sorted, output the new CSV file
    with open(outputPath, "w") as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow(['NUM_ENQ','age','category','UTC','subservice','Other diseases'])
        for patient in patients.values():
            for utc, category, subservice in patient:
                writer.writerow([patient.patientID, patient.age, category, utc, subservice, ''])


# The Backend of ParcoursURGE

## The datasets

We provide a default simulated dataset in parser/test/. Read the [parser README](./parser/README.md) to know how to preparse this datasets.

## The Backend server

You first need to create the C/C++--Python binding of the module "parcoursprog":

```
make parcoursproj
```

Read the [parser README](./parser/README.md) then to launch the local server.

## Generate synthetic datasets

1. Compile the python module "parcoursprog" if it was not done before:
```
make parcoursproj
```

2. Generate the aggregate tree of the dataset of interest by running:

```
python ./generate_data/aggregate.py [data_directory] [--output JsonOutput]
```

3. Generate a new dataset that follows the same distribution as the aggregated tree generated above by running:

```
python ./generate_data/generate.py [aggregated_json] [n_patients]
```

The default values are:
- aggregated\_json: data/data.json
- n\_patients: 100

This python script generates a CSV file to preparse using the preparser described [here](./parser/README.md)

## A Docker image toolchain

Because of some constraints, it may be necessary to export all the compilation toolchains into a docker image, and to use this docker image to build all the components of the backend of parcoursvis (pparser, preparser, and the python module).
toolchain.Dockerfile allows to create that docker image. It will then use the bash script "compile.sh" to compile parcoursvis and its subcomponents.

For the document image to work, the user needs to mount this directory (back) to target the /src directory of the docker container:

```
docker build -f toolchain.Dockerfile -t parcoursurge-toolchain-back .   #Build the docker image, supposing the user is in the back directory
docker run -v <ABSOLUTE_PATH_TO_BACK>:/src parcoursurge-toolchain-back  #This command mounts the back directory to target /src on the docker container
```

Then, the user can modify the PYTHONPATH environment variable to use the parcoursprog python module, or to install manually the generated eggs to your default site-packages directory, for example:

```
PYTHONPATH=$PWD python parser/index.py
```

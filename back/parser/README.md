1. Compilation

- To compile the programs (preparser and deparser):

```
make
```

- To compile the tests, you will need: jsoncpp, cmake, pkgconfig, and gtest:
```
mkdir build
cmake . -G "Unix Makefiles" 
```

2. Usage

- To run preparse the dataset:
```
./preparser filename.csv
```
or 
```
bunzip2 < filename.csv.bz2 | ./preparser
```

- To run the parser:
```
./pparser
```

- To run the server:
```
python index.py
```

Note that you need to install the C/C++--Python binding of parcoursprog for the server to work. See the [backend README](../README.md) to know how to compile it.

- To run the test, do the following:

```
cd parser/build #to run if you are not yet in the build directory created when you compile the tests using CMake
ln -s ../tests .
./pparserUnitTests
```

We rely on "gcov" for the code coverage. To retrieve the code coverage, do:
```
cd parser/build #to run if you are not yet in the build directory
./parser/coverage.sh #To adapt depending on where you are in the folder structure
xdg-open lcov-report/index.html #Can be replaced with a firefox/chromium/whatever browser command to open the HTML file rapport/index.html
```

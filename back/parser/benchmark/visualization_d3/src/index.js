import * as d3 from "d3";
const jsonData0 = require("./data0.json");
const jsonData1 = require("./data1.json");
const jsonData  = [jsonData0, jsonData1];


//------------------------------------------------------------------------------
//------------------------Declare common GUI components-------------------------
//------------------------------------------------------------------------------

//The SVG
var svg = d3.select("#svgcontainer").append("svg")
            .attr("width", 1920)
            .attr("height", 720);

//The widgets
var   curDatabaseBtn        = null;
var   curSortRadioBtn       = null;
const hysteresisInertiaTxt  = document.getElementById("hysteresisInertia");
const percentageAccuracyTxt = document.getElementById("percentageAccuracy");
const iterationIDBtn        = document.getElementById("iterationID");
const checkTopologyBtn      = document.getElementById("checkTopology");
const thresholdTxt          = document.getElementById("threshold");
const chunkSizeTxt          = document.getElementById("chunkSize");

var updateVisualization = function() {
  console.log('Updating the visualization in-progress...');
  console.log(`Current sorting algorithm: ${curSortRadioBtn.value}`);
  console.log(`Results of iteration: ${iterationIDBtn.value}`);
  console.log(`Topology checked: ${checkTopologyBtn.checked}`);
  console.log('');

  let data = jsonData[curDatabaseBtn.value];

  //Update the metadata UI
  thresholdTxt.textContent = data.Progressive[iterationIDBtn.value].threshold;
  chunkSizeTxt.textContent   = data.Progressive[iterationIDBtn.value].chunk_size;

  //Recreate the hierarchy
  let id = 0;
  let root = {
    name:          "root",
    method:        curSortRadioBtn.value,
    checkTopology: checkTopologyBtn.checked,
    children:      [],
    id:            id++,
  };

  for(let stats of data.Progressive[iterationIDBtn.value].stats) {
    if(stats.sortingMethod == curSortRadioBtn.value) {
      let x = 0;
      root.children.push({
        name:     "iteration",
        depth:    stats.depth,
        children: stats.data.map(function(s){ return {...s, id: id++, x: x++}; }),
        id:       id++,
        stableAt: [stats.stableWithoutTopologyAtIter, stats.stableWithTopologyAtIter],
      });
    }
  }

  let hierarchy = d3.hierarchy(root);
  let leaves    = hierarchy.leaves();
  let maxBadStatistics = Math.max(1, ...leaves.map((d) => d.data.badStatistics));
  let maxPermutations  = Math.max(1, ...leaves.map((d) => d.data.permutations));

  //Draw the hierarchy
  let outerWidthPerItem    = 40;
  let innerMaxWidthPerItem = 40;
  let innerMinWidthPerItem = 30;

  svg.selectAll("g").remove();
  let depths = svg.selectAll("g").data(hierarchy.children, d => `${curSortRadioBtn.value}${d.id}`).join(
    enter => {
      let g = enter.append("g")
        .attr('transform', d => `translate(5,${d.data.depth*outerWidthPerItem+5})`);

      g.selectAll("g").data(d => d.children, d => `${curSortRadioBtn.value}${d.id}`).join(
        enter => {
          var g = enter.append("g")
            .attr('transform', d => `translate(${d.data.x * outerWidthPerItem + (outerWidthPerItem-innerMaxWidthPerItem)/2},0)`);
          g.append("ellipse")
            .attr('cx', innerMaxWidthPerItem/2)
            .attr('cy', innerMaxWidthPerItem/2)
            .attr('rx', d => (innerMinWidthPerItem + d.data.permutations  * (innerMaxWidthPerItem-innerMinWidthPerItem)/maxPermutations)/2)
            .attr('ry', d => (innerMinWidthPerItem + d.data.badStatistics * (innerMaxWidthPerItem-innerMinWidthPerItem)/maxBadStatistics)/2)
            .attr('fill', d => (d.data.permutations == 0 && d.data.badStatistics == 0 && (root.checkTopology ? d.data.missingNodes == 0 : true)) ? 'green' : 'red')
            .attr('stroke', d => (d.parent.data.stableAt[root.checkTopology ? 1:0] == d.data.x) ? "black" : "")
            .attr('stroke-width', 3);
          g.append("text")
            .attr('x', innerMaxWidthPerItem/2).attr('y', innerMaxWidthPerItem/2)
            .attr('dominant-baseline', 'central')
            .attr('text-anchor', 'middle')
            .text(d => `${d.data.permutations}/${d.data.badStatistics}`)
        }
      );
    }
  );
};

var updateDatabase = function() {
  let data = jsonData[curDatabaseBtn.value];

  //Database information
  hysteresisInertiaTxt.textContent  = data.GeneralParameters.hysteresisInertia;
  percentageAccuracyTxt.textContent = data.GeneralParameters.percentageAccuracy;

  //Iteration IDs
  const iterationLabelMin = document.getElementById("iterationLabelMin");
  const iterationLabelMax = document.getElementById("iterationLabelMax");

  iterationLabelMin.textContent = 0;
  iterationLabelMax.textContent = data.Progressive.length-1;

  iterationIDBtn.min   = 0;
  iterationIDBtn.max   = data.Progressive.length-1;
  if(parseInt(iterationIDBtn.value) > iterationIDBtn.max)
      iterationIDBtn.value = iterationIDBtn.max;

  updateVisualization();
};


//------------------------------------------------------------------------------
//----------------------------Handle GUI components-----------------------------
//------------------------------------------------------------------------------

//Database
const dataBtns = document.querySelectorAll('input[name="data"]');

for(let btn of dataBtns) {
  if(btn.checked)
    curDatabaseBtn = btn;
  btn.addEventListener('change', function() {
    if(this !== curDatabaseBtn)
      curDatabaseBtn = this;
    updateDatabase();
  });
}

//Sorting algorithm
const sortRadioBtns = document.querySelectorAll('input[name="sort"]');

for(let btn of sortRadioBtns) {
  if(btn.checked)
    curSortRadioBtn = btn;
  btn.addEventListener('change', function() {
    if(this !== curSortRadioBtn)
      curSortRadioBtn = this;
    updateVisualization();
  });
}

//Iteration IDs
iterationIDBtn.value = 0;
iterationIDBtn.addEventListener('change', function() {
  if(parseInt(iterationIDBtn.value) > iterationIDBtn.max)
    iterationIDBtn.value = iterationIDBtn.max;
  else if(iterationIDBtn.value < 0)
    iterationIDBtn.value = 0;
  updateVisualization();
});


//Check Topology
checkTopologyBtn.addEventListener('change', function() {
  updateVisualization();
});

//Update the visualization at least once
updateDatabase();

#include "pparser.hpp"

using namespace std;

int main(int argc, char* argv[])
{
    context ctx;
    const char * dir = "./tmp";
    bool print_histograms = false;

    if (argc <= 1) {
	cerr << "syntax: " << argv[0] << " <data_directory> [-v]" << endl;
	return 1;
    }
    dir = argv[1];
    if (argc > 2 && argv[2][0]=='-' && argv[2][1]=='h') {
        print_histograms = true;
    }

    chrono::steady_clock::time_point begin = chrono::steady_clock::now();
    try {
      ctx.load(dir);
    }
    catch(const runtime_error& err) {
      cerr << err.what() << endl;
      return 1;
    }
    chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    cout << "Load time = "
         << double(chrono::duration_cast<chrono::microseconds>(end - begin).count()) / 1e6
         << "s" << endl;

    begin = chrono::steady_clock::now();
    ctx.process_all();
    end = chrono::steady_clock::now();
    cout << "Process time = "
         << double(chrono::duration_cast<chrono::microseconds>(end - begin).count()) / 1e6
         << "s" << endl;

    ofstream ftree("./data2.json");
    begin = chrono::steady_clock::now();
    ctx.print(ftree, print_histograms);
    ftree.flush();
    end = chrono::steady_clock::now();
    cout << "Save time = "
         << double(chrono::duration_cast<chrono::microseconds>(end - begin).count()) / 1e6
         << "s" << endl;
    return 0;
}

#ifndef BARCHART_HPP
#define BARCHART_HPP

#include <vector>
#include <iostream>
#include <sstream>
#include <cstdint>
#include <algorithm>

/** \brief  Basic structure to handle barcharts*/
struct barchart {
    std::vector<unsigned>    _bars;       /*!< Each bar values*/
    std::vector<std::string> _categories; /*!< The names of each bar*/

    /** \brief  Constructor, initialize a barchart with 'size' categories
     * \param size the number of categories of the barchart */
    barchart(size_t size=0) {
        resize(size);
    }

    /** \brief  Reset all the categories of the barchart to 0. The size and the name of the categories are preserved*/
    void clear() {
        std::fill(_bars.begin(), _bars.end(), 0);
    }

    /** \brief  Resize the bar chart. All its values will be reset to 0
     * The new categories will be names "i", "i" being its index
     * \param size the new size */
    void resize(size_t size) {
        _bars.resize(size);
        unsigned old_size = _categories.size();
        _categories.resize(size);
        if(old_size < size) {
            unsigned i = old_size+1;
            for(auto it = _categories.begin()+old_size+1; it != _categories.end(); it++, i++)
                (*it) = std::to_string(i);
        }
        clear();
    }

    /** \brief  Add multiple values at once using a mask variable.
     * \param mask The mask to apply. 0b101 is equivalent to "add(0); add(2);"
     * \return  true if the mask can be applied. This method works only if the barchart has less (or equal) to 32 values*/
    bool add_mask(unsigned mask) {
        if(_bars.size() > 32)
            return false;

        for (size_t i = 0; mask != 0 && i < _bars.size(); i++) {
             if (mask&1)
                 _bars[i]++;
             mask >>= 1;
        }
        return true;
    }

    /** \brief Do a "+1" to the category "index"
     * \param index the index to add a value in the barchart
     * \return true if index is valid, false otherwise */
    bool add(unsigned index, int count=1) {
        if(index >= _bars.size())
            return false;
        _bars[index] += count;
        return true;
    }

    /** \brief  Set the value of the barchart at index "index"
     * \param index the index to set the value to
     * \param value the value of the barchart
     * \return  true if index is within range, false otherwise */
    bool set_value_at(unsigned index, unsigned value) {
        if(index >= _bars.size())
            return false;
        _bars[index] = value;
        return true;
    }

    /** \brief  Get the size of the barchart
     * \return  The size (i.e., number of categories) of the barchart */
    unsigned size() const {
        return _bars.size();
    }

    /** \brief Set the name of the category at index "index" to "name"
     * \param index the index of the category
     * \param name the name to use
     * \return false if "index" is invalid */
    bool set_category_name(unsigned index, const std::string& name)
    {
        if(index >= _bars.size())
            return false;
        _categories[index] = name;
        return true;
    }

    /** \brief  Print the barchart in a JSON format
     * \param out the output stream to print the barchart data into
     * \return the parameter "out" */
    std::ostream& print(std::ostream& out) const {
         if (_bars.empty()) {
              out << "[]";
              return out;
         }

        //To ease the printing (due to the "comma" of JSON format)
        auto print_bar = [](const std::string& name, uint32_t value, std::ostream& _out) -> std::ostream& {
            _out << "{ \"name\": \"" << name << "\", \"value\": " << value << "}"; 
            return _out;
        };
        out << "[" << std::endl;
        for(int32_t i = 0; i < (int32_t)(_bars.size())-1; i++)
            print_bar(_categories[i], _bars[i], out) << "," << std::endl; //Pay attention to commas
        print_bar(_categories.back(), _bars.back(), out) << std::endl;    //Print the last one
        out << "]" << std::endl;
        return out;
    }

    /** \brief  Return the barchart into a JSON format
     * \return   a string containing the barchart data */
    std::string str() const {
        std::ostringstream out;
        print(out);
        return out.str();
    }
     
};

#endif

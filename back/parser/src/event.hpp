#ifndef EVENT_HPP
#define EVENT_HPP

#include "info.hpp"

/** \brief  Basic Event structure. */
struct Event {
    uint32_t name; /*!< ID of the name of the event*/
    uint64_t type; /*!< The composed type (bitmap) of the event*/
    uint64_t drug; /*!< The different drugs (bitmap) associated with the event*/
    uint64_t start_date; /*!< When does the event start? (in seconds)*/
    uint64_t end_date;   /*!< When does the event finish? (in seconds)*/
    uint8_t age;         /*!< The age of the individual associated with the event*/
    Event():
      name(0), type(0), drug(0), start_date(0), end_date(0), age(0) 
    { }
    Event(const Info& info):
        name(info.name),
        type(1U << info.type),
        drug(info.drug),
        start_date(info.seconds), 
        end_date(info.seconds),
        age(info.age) { }
    void operator=(const Info& info) {
        name = info.name;
        type = 1U << info.type;
        drug = info.drug;
        start_date = info.seconds;
        end_date = info.seconds;
        age = info.age;
    }
    Event(uint32_t _name, uint8_t _type, uint32_t _drug,
          uint16_t _start_date, uint16_t _end_date, uint8_t _age):
        name(_name),
        type(1U << _type),
        drug(_drug),
        start_date(_start_date),
        end_date(_end_date),
        age(_age) { }
    void clear() {
        type = 0;
        drug = 0;
        start_date = 0;
        end_date = 0;
        age = 0;
    }
    bool valid() { return end_date >= start_date; }
    bool contains_type(int t) const { return type & (1U << t); }
    bool contains_drug(int d) const { return drug & (1U << d); }
    void add_type(int t) { type |= (1U << t); }
    void add_drug(int d) { drug |= (1U << d); }
//    void add_drug(int d) { drug = d;}
};

inline bool operator==(const Event& ev1, const Event& ev2) {
    return ev1.name       == ev2.name &&
           ev1.type       == ev2.type &&
           ev1.drug       == ev2.drug &&
           ev1.start_date == ev2.start_date &&
           ev1.end_date   == ev2.end_date   &&
           ev1.age        == ev2.age;
}

inline bool operator!=(const Event& ev1, const Event& ev2) {
    return !(ev1 == ev2);
}

#endif

#include <iostream>
#include <stdexcept>
#include "mmap_mem.hpp"

#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

mmap_mem::mmap_mem(const char * filename) 
  : mmap_mem(::open(filename, O_RDONLY)) {
}

mmap_mem::mmap_mem(int fd)
    : _fd(fd),
    _size(0), _data(nullptr) {
  if (_fd < 0)
    throw std::runtime_error("Cannot open file");
  _size = lseek(_fd, 0, SEEK_END);     
  _data = mmap(NULL, _size, PROT_READ, MAP_FILE | MAP_SHARED, _fd, 0);
  if (_data == MAP_FAILED)
      throw std::runtime_error("Cannot map file");
}

mmap_mem::~mmap_mem() {
    if (_data)
        munmap(_data, _size);
    if (_fd >= 0)
        ::close(_fd);
    _data = nullptr;
    _fd = -1;
}

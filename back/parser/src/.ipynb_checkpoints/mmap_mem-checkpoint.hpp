#ifndef MMAP_MEM_HPP
#define MMAP_MEM_HPP

#include <iostream>

class mmap_mem {
private:
    int _fd;
    std::size_t _size;
    void *_data;  
public:
    mmap_mem(const char * filename);
    mmap_mem(int fd);
    ~mmap_mem();

    std::size_t size() const { return _size; }
    /*!
     * Gets a read-only pointer to the mapped data.
     */
    const void* operator*() const { return _data; }
    const void* data() const { return _data; }
    void* operator*() { return _data; }
    void* data() { return _data; }
};

#endif

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <sys/stat.h>
#include "info.hpp"
#include "mmap_mem.hpp"
#include "date_delta.hpp"

using namespace std;

vector<string> names, types, drugs, diseases;
const mmap_mem *     info_mem           = nullptr;
const Info *         info_table         = nullptr;
size_t               info_size          = 0;
const mmap_mem *     indi_mem           = nullptr;
const IndiInfo *     indi_table         = nullptr;
size_t               indi_size          = 0;
bool                 verbose            = false;

int deparse(ostream& fout) {
     const char sep = ',';
     const size_t has_diseases = diseases.size();
     fout << "NUM_ENQ,age,category,date,drug";
     if (has_diseases)
          fout << ",Other Diseases";
     fout << "\r\n"; // force CR LF for windows compat
     for (size_t i = 0; i < info_size; i++) {
          const Info& info = info_table[i];
          fout << names[info.name] << sep 
               << int(info.age) << sep
               << types[info.type] << sep 
               << info.seconds << sep
               << drugs[info.drug];
          if (has_diseases) {
               fout << sep;
               const IndiInfo& indi = indi_table[info.name];
               uint8_t disease = indi.diseases;
               for (size_t j = 0; j < has_diseases && disease != 0; j++) {
                    if (disease&1) {
                        fout << diseases[j];
                        disease >>= 1;
                        if (disease)
                             fout << ' ';
                    }
                    else
                        disease >>= 1;
               }
          }
          fout << "\r\n";
     }
     return 0;
}

static bool file_exists(const string& fileName)
{
    std::ifstream infile(fileName.c_str());
    return infile.good();
}

void read_dicts(const char * filename) {
     ifstream fdict(filename, ios::binary);
     if (! fdict) {
          throw runtime_error(string("File ")+string(filename)+" does not exist");
     }
     ::read_dicts(fdict, types, drugs, names, diseases);
     if (verbose) {
         cerr << "Types: " << types.size() << endl;
         cerr << "Drugs: " << drugs.size() << endl;
         cerr << "Names: " << names.size() << endl;
         cerr << "Diseases: " << diseases.size() << endl;
     }
}

void load(const char * dir) {
     string string_file(dir);
     string_file += "/data.map";
     if (! file_exists(string_file))
          throw runtime_error(string("File ")+string_file+" does not exist");
     string info_file(dir);
     info_file += "/data.bin";
     if (! file_exists(info_file))
          throw runtime_error(string("File ")+info_file+" does not exist");
     read_dicts(string_file.c_str());
     info_mem = new mmap_mem(info_file.c_str());
     info_table = reinterpret_cast<const Info *>(info_mem->data());
     if (info_table[0] != INFO_V1)
          throw runtime_error("Invalid info file version");
     else if (verbose)
          cerr << "# info table have version 1" << endl;
     info_table++;
     info_size = info_mem->size() / sizeof(Info) - 1;
     if (! diseases.empty()) {
        string indi_file(dir);
        indi_file += "/indi.bin";
        if (! file_exists(indi_file))
            throw runtime_error(string("File ")+indi_file+" does not exist");
        indi_mem = new mmap_mem(indi_file.c_str());
        indi_table = reinterpret_cast<const IndiInfo *>(indi_mem->data());
        if (indi_table[0] != INDI_INFO_V1)
            throw runtime_error("Invalid individual info file version");
        else if (verbose)
            cerr << "# individual info table have version 1" << endl;
        indi_table++;
        indi_size = indi_mem->size() / sizeof(IndiInfo);
     }
     if (verbose)
       cerr << "# context loaded from "<< dir << endl;
}


int main(int argc, char* argv[]) {
     const char * dir = "tmp";
     int ret = 0;

     if (argc > 1)
          dir = argv[1];

     if (string(argv[argc-1]) == "-v") {
       verbose = true;
     }

     ostream * out = &cout;
     // if (argc > 2) {
     //      out = new ofstream(argv[2], ios_base::binary);
     //      if (! *out) {
     //           cerr << "Cannot open " << argv[2] << endl;
     //           return 1;
     //      }
     // }
     try {
          load(dir);
          ret = deparse(*out);
     }
     catch(std::runtime_error& err) {
          cerr << "Error while deparsing: " << err.what() << endl;
     }
     if (out != &cout) {
          out->flush();
          delete out;
     }
     return ret;
}

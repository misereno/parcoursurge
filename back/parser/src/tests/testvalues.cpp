#include <iostream>
#include <gtest/gtest.h>
#include "pparser.hpp"

class ValuesTest : public testing::Test {
    protected:
        ValuesTest() {}

        void SetUp() override {
        }

        Values value;
};

TEST_F(ValuesTest, init) {
    EXPECT_TRUE(value.empty());
    EXPECT_EQ(value.min(),    0);
    EXPECT_EQ(value.max(),    0);
    EXPECT_EQ(value.mean(),   0);
    EXPECT_EQ(value.median(), 0);
}

TEST_F(ValuesTest, sort) {
    EXPECT_TRUE(value.sorted);
    for(int i = 0; i < 10; i++)
        value.add(i, true);
    EXPECT_TRUE(value.sorted);

    value.add(0, true);
    EXPECT_TRUE(value.sorted); //Sorted by insertion
                               
    value.add(0, false);
    EXPECT_FALSE(value.sorted);
    EXPECT_EQ(value.back(), 0);

    value.sort();
    EXPECT_TRUE(value.sorted);
    for(size_t i = 0; i < value.size()-1; i++)
        EXPECT_LE(value[i], value[i+1]);
}

TEST_F(ValuesTest, metric) {
    for(int i = 0; i < 10; i++)
        value.add(2*i, true);

    EXPECT_EQ(0,  value.min());
    EXPECT_EQ(18, value.max());
    EXPECT_EQ(9,  value.mean());
    EXPECT_EQ(9,  value.median());

    value.add(20, true);
    EXPECT_EQ(10, value.median());
    EXPECT_TRUE(value.sorted);

    value.add(0, true);
    EXPECT_TRUE(value.sorted);
    //Test min/max when sorted==false
    EXPECT_EQ(0,  value.min());
    EXPECT_EQ(20, value.max());

    EXPECT_EQ(9, value.median());
}

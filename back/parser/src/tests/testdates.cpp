#include <iostream>
#include <iomanip>
#include <sstream>
#include "info.hpp"

using namespace std;

int main(int argc, char * argv[]) {
  for (int i = 1; i < argc; i++) {
    int year, month, day;
    char delim;
    istringstream is(argv[i]);
    is >> year >> delim >> month >> delim >> day;
    int days = date_to_days(year, month, day);
    time_t time = days_to_seconds(days);
    struct tm * t = localtime(&time);
    cout << (t->tm_year+1900) << '-' 
         << setfill('0') << setw(2) 
         << (t->tm_mon+1) << '-' 
         << setfill('0') << setw(2) 
         << t->tm_mday << endl;
  }
  return 0;
}

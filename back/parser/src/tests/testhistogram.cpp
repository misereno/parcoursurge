#include <iostream>
#include <algorithm>
#include <gtest/gtest.h>
#include <json/value.h>
#include <json/reader.h>
#include "histogram.hpp"

class HistogramTest : public testing::Test {
    public:
        HistogramTest() : hist(10, 0.0f, 9.0f) {}

    protected:
        void SetUp() override {
        } 

        histogram<float> hist; 
};

TEST_F(HistogramTest, invalid) {
    unsigned temp;
    EXPECT_FALSE(hist.get_bin_index_of(hist.max()+1.0f, temp));

    hist.compute(std::vector<float>()); //empty data
    EXPECT_EQ(hist.count, 0);

    hist.set_range(0, 0); //min == max
    hist.compute(std::vector<float>({0, 1, 2, 3, 4})); //min == max
    EXPECT_EQ(hist.count, 0);
    EXPECT_EQ(hist.median(), hist.min());
}

TEST_F(HistogramTest, clear) {
    float min = hist.min();
    float max = hist.max();

    hist.compute(std::vector<float>{0, 1, 2, 3});
    EXPECT_EQ(hist.count, 4);

    hist.clear();

    EXPECT_EQ(hist.count, 0);
    EXPECT_EQ(hist.cumsum, 0.0f);
    EXPECT_EQ(hist.min(), min);
    EXPECT_EQ(hist.max(), max);
}

TEST_F(HistogramTest, set_range) {
    EXPECT_FALSE(hist.set_range(10, 0));
    EXPECT_LE(hist.min(), hist.max()); //Check that values are reversed
    EXPECT_EQ(hist.min(), 0);
    EXPECT_EQ(hist.max(), 10);

    EXPECT_TRUE(hist.set_range(0, 0));

    hist.set_range(std::vector<float>({0, 2, 5, -1, -4, 10, 2, 20}));
    EXPECT_EQ(hist.min(), -4);
    EXPECT_EQ(hist.max(), 20);
}

TEST_F(HistogramTest, stats) {
    for(auto i : {9U, 20U})
    {
        hist.bins.resize(i+1);
        EXPECT_TRUE(hist.set_range(0, i));
        hist.clear();

        //Even number added
        float range   = (hist.max() - hist.min());
        float sizeBar = range / hist.bins.size();
        float halfBar = sizeBar / 2.0f;

        //Test with only one bin being "full"
        hist.compute(std::vector<float>({0, 0, 0, 0, 0}));
        EXPECT_FLOAT_EQ(halfBar, hist.median());
        hist.clear();

        //Test with all bins being full "twice"
        std::vector<float> fullRange;
        for(uint32_t k = 0; k < 2; k++)
            for(uint32_t j = 0; j <= i; j++)
                fullRange.push_back(j);
        hist.compute(fullRange);
        for(auto x : hist.bins)
            EXPECT_EQ(x, 2);
        unsigned index;
        EXPECT_TRUE(hist.get_bin_index_of(range/2.0f, index));
        EXPECT_FLOAT_EQ(index*range/hist.bins.size() + halfBar, hist.median_old()); //normally median == average. But we are doing histogram medians (i.e., which bin contains the median).
        EXPECT_FLOAT_EQ(range/2.0f, hist.mean());

        hist.clear();

        //Odd number added
        hist.compute(std::vector<float>({0, 1, 2, 3, 4, 5, 6, 7, 8}));
        EXPECT_FLOAT_EQ(4*sizeBar + halfBar, hist.median_old()); //Center of the fifth bar
        std::vector<float> vec({0, 1, 2, 3, 4, 5, 6, 7, 8});
        size_t n = vec.size() / 2;
        std::nth_element(vec.begin(), vec.begin()+n, vec.end());
        int exact_median = vec[n];
        hist.clear();
        hist.compute(vec);
        float approx_median = hist.median();
        float approx_median2 = hist.median_old();
        EXPECT_FLOAT_EQ(4*sizeBar + halfBar, approx_median2); //Center of the fifth bar
        EXPECT_TRUE(std::abs(exact_median - approx_median) <= std::abs(exact_median - approx_median2));
        EXPECT_FLOAT_EQ(4.0f, hist.mean());

    }
}

TEST_F(HistogramTest, print) {
    //Initialize the histogram with default values
    hist.bins.resize(20U);
    hist.set_range(0, 20U);
    hist.compute(std::vector<float>({0, 1, 2, 3, 4, 5, 6, 7, 8}));

    //Check the printing
    std::ostringstream out;
    hist.print(out);

    //Check that str and print returns the same thing
    EXPECT_EQ(hist.str(), out.str());

    //Check that we can parse the resulting JSON str
    std::string str = "{\"data\": " + out.str() + "}";
    Json::Reader jReader;
    Json::Value  jRoot;
    ASSERT_TRUE(jReader.parse(str.c_str(), jRoot));

    EXPECT_EQ(jRoot["data"]["range"].size(), 2);
    EXPECT_FLOAT_EQ(jRoot["data"]["range"][0].asFloat(), hist.min());
    EXPECT_FLOAT_EQ(jRoot["data"]["range"][1].asFloat(), hist.max());

    ASSERT_EQ(jRoot["data"]["bins"].size(), hist.size());
    for(unsigned j = 0; j < hist.size(); j++)
        EXPECT_EQ(jRoot["data"]["bins"][j].asInt(), hist.bins[j]);

    EXPECT_NEAR(jRoot["data"]["median"].asFloat(), hist.median(), 1e-4);
    EXPECT_NEAR(jRoot["data"]["mean"].asFloat(), hist.mean(), 1e-4);
}

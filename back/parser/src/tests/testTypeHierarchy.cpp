#include <iostream>
#include <gtest/gtest.h>
#include "pparser.hpp"

class TypeHierarchyTest : public testing::Test {
    protected:
        TypeHierarchyTest() {}

        void SetUp() override {

        }

        TypeHierarchy type_hierarchy;
};

TEST_F(TypeHierarchyTest, init) {
    for(uint32_t i = 0; i < 2; i++) {
        EXPECT_EQ(type_hierarchy.nodes.size(), 1);
        EXPECT_EQ(type_hierarchy.get_root(), &type_hierarchy.nodes[0]);
        EXPECT_EQ(type_hierarchy.get_root()->get_parent(), TypeHierarchyNode::NO_PARENT);
        EXPECT_EQ(type_hierarchy.get_root()->get_children().size(), 0);
        EXPECT_EQ(type_hierarchy.find_first_node_by_name("root"), type_hierarchy.get_root());

        //redo the test after a clear
        //other unit test function will test that those children were actually correctly added
        TypeHierarchyNode* child1 = type_hierarchy.new_node(type_hierarchy.get_root(), "child1");
        type_hierarchy.new_node(child1, "child2");
        type_hierarchy.clear();
    }

    EXPECT_EQ(type_hierarchy.get_node(42), nullptr);
    EXPECT_EQ(type_hierarchy.get_node(-1), nullptr);

    EXPECT_EQ(type_hierarchy.find_first_node_by_name("foo"), nullptr);
    EXPECT_EQ(type_hierarchy.find_first_node_by_type_combination(0xff), nullptr);
}

TEST_F(TypeHierarchyTest, add_child) {
    TypeHierarchyNode* child1 = type_hierarchy.new_node(type_hierarchy.get_root(), "child1");
    child1->type_combination = 0x01;
    EXPECT_EQ(type_hierarchy.get_root()->get_children()[0], type_hierarchy.node_id(child1));    
    EXPECT_EQ(child1->get_parent(), type_hierarchy.node_id(type_hierarchy.get_root()));
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("child1"), child1);
    EXPECT_EQ(type_hierarchy.find_first_node_by_type_combination(0x01), child1);

    TypeHierarchyNode* child2 = type_hierarchy.new_node(child1, "child1"); //Same name
    child2->type_combination = 0x01;
    child1 = &type_hierarchy.nodes[type_hierarchy.get_root()->get_children()[0]]; //Previous pointer was invalid due to new node allocation
    EXPECT_EQ(child2->get_parent(), type_hierarchy.node_id(child1));
    EXPECT_EQ(type_hierarchy.find_first_node_by_name("child1"), child1);         //Test "same name"
    EXPECT_EQ(type_hierarchy.find_first_node_by_type_combination(0x01), child1); //Test "same type"

    EXPECT_EQ(type_hierarchy.nodes.size(), 3); //root + child1 + child2
}

TEST_F(TypeHierarchyTest, change_parent) {
    TypeHierarchyNode* child1 = type_hierarchy.new_node(type_hierarchy.get_root(), "child1");
    uint64_t child1ID = type_hierarchy.node_id(child1);
    TypeHierarchyNode* child2 = type_hierarchy.new_node(child1, "child2");
    child1 = &type_hierarchy.nodes[child1ID];

    type_hierarchy.set_relationship(type_hierarchy.get_root(), child2);

    for(uint32_t i = 0; i < 2; i++) {
        EXPECT_EQ(type_hierarchy.get_root()->get_children().size(), 2);
        EXPECT_EQ(child1->get_children().size(), 0);
        EXPECT_EQ(child1->get_parent(), type_hierarchy.node_id(type_hierarchy.get_root()));
        EXPECT_EQ(child2->get_parent(), type_hierarchy.node_id(type_hierarchy.get_root()));
        type_hierarchy.set_relationship(type_hierarchy.get_root(), child2); //Redo a loop without modifying the hierarchy, as this parent was already set.
    }

    EXPECT_EQ(type_hierarchy.nodes.size(), 3); //root + child1 + child2
}

TEST_F(TypeHierarchyTest, is_aggregation_of) {
    TypeHierarchyNode* child1 = type_hierarchy.new_node(type_hierarchy.get_root(), "child1");
    child1->type_combination = 0x01;
    uint64_t child1ID = type_hierarchy.node_id(child1);
    TypeHierarchyNode* child2 = type_hierarchy.new_node(child1, "child2");
    child2->type_combination = 0x02;
    child1 = &type_hierarchy.nodes[child1ID];

    EXPECT_TRUE(type_hierarchy.is_aggregation_of(0x01, 0x01));  //child1 is an aggregation of child1 (same type)
    EXPECT_TRUE(type_hierarchy.is_aggregation_of(0x01, 0x02));  //child1 is an aggregation of child2
    EXPECT_FALSE(type_hierarchy.is_aggregation_of(0x02, 0x01)); //child2 is a child of child1, hence not an aggregation
    EXPECT_FALSE(type_hierarchy.is_aggregation_of(0x01, 0x03)); //0x03 does not exist
}

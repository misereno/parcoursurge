#include <iostream>
#include <gtest/gtest.h>
#include <json/value.h>
#include <json/reader.h>
#include "barchart.hpp"

class BarchartTest : public testing::Test {
    protected:
        void SetUp() override {

        }

        barchart chart;
};

TEST_F(BarchartTest, invalid) {
    EXPECT_FALSE(chart.add(0));
    EXPECT_FALSE(chart.set_value_at(0, 32));
    EXPECT_FALSE(chart.set_category_name(0, "foo"));

    chart.resize(128);
    EXPECT_FALSE(chart.add(256));
    EXPECT_FALSE(chart.set_value_at(256, 32));
    EXPECT_FALSE(chart.set_category_name(256, "foo"));
    EXPECT_FALSE(chart.add_mask(0b11));
}

TEST_F(BarchartTest, add) {

    chart.resize(1);
    for(uint32_t i = 0; i < 10; i++)
        chart.add(0);
    EXPECT_EQ(chart._bars[0], 10);
    chart.set_category_name(0, "cat");
    
    chart.resize(2);
    EXPECT_EQ(chart._bars[0], 0);
    EXPECT_EQ(chart._categories[0], "cat");

    chart.add_mask(0b11);
    for(uint8_t i=0; i < 2; i++)
        EXPECT_EQ(chart._bars[i], 1);
}

TEST_F(BarchartTest, print) {
    EXPECT_EQ(chart.str(), "[]");

    chart.resize(10);
    for(uint32_t i = 0; i < 10; i++) {
        chart.set_value_at(i, 2*i);
        chart.set_category_name(i, "category"+std::to_string(i));
    }
    std::ostringstream out;
    chart.print(out);

    //Check that str and print returns the same thing
    EXPECT_EQ(chart.str(), out.str());

    //Check that we can parse the JSON str
    std::string str = "{\"data\": " + out.str() + "}";
	Json::Reader jReader;
	Json::Value  jRoot;
    ASSERT_TRUE(jReader.parse(str.c_str(), jRoot));

    //Check that we have all the data in there
    EXPECT_EQ(jRoot["data"].size(), chart.size());

    for(uint32_t i = 0; i < 10; i++) {
        EXPECT_EQ(jRoot["data"][i]["name"].asString(), "category"+std::to_string(i));
        EXPECT_EQ(jRoot["data"][i]["value"].asInt(),   2*i);
    }
}

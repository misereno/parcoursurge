#include <iostream>
#include <gtest/gtest.h>
#include "pparser.hpp"

class NodeTest : public testing::Test {
    protected:
        NodeTest() : events({"fivealpha", "alphabloc", "phyto", "surgery", "death", "interruption", "no treatment"}) {}
        void SetUp() override {
            ctx.load("./tests");
        }
        
        context ctx;
        Node    root;
        std::vector<std::string> events;
};

TEST_F(NodeTest, beg) {
    EXPECT_EQ(root.count, 0);
    EXPECT_TRUE(root.subcount.empty());
    EXPECT_TRUE(root.children.empty());
}

TEST_F(NodeTest, clear) {
    //Add stuff in root
    root.children[0] = 1;
    root.children[1] = 2;
    root.subcount[0] = 0;
    root.subcount[1] = 0;
    root.count = 2;

    EXPECT_EQ(root.count, 2);
    EXPECT_EQ(root.subcount.size(), 2);
    EXPECT_EQ(root.children.size(), 2);

    //Clear
    root.clear();

    EXPECT_EQ(root.count, 0);
    EXPECT_TRUE(root.subcount.empty());
    EXPECT_TRUE(root.children.empty());
}

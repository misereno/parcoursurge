FILE=data/file.csv
FILEBZ=data/file.csv.bz2

if [ ! -f tmp/data.bin ]; then
    if test -f $FILE; then
        echo "Parse .csv file..."
        cat $FILE | ./preparser -

    elif test -f $FILEBZ; then
        echo "Parse .csv.bz2 file..."
        bzcat $FILEBZ | ./preparser -
    fi
else
    echo "file tmp/data.bin already exists; skipping preparser. If you want to force the preparser, delete tmp/data.bin"
fi

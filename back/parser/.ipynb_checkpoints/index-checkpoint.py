import sys
import os
import asyncio
import websockets
import json
import logging

import parcoursprog
from ParcoursvisClient import *

logging.basicConfig()
connected = set()
state_context = {}
data_directory = "tmp"

def str_to_DiseaseFilterMode(name):
    if(name == "both"):
        return parcoursprog.DiseaseFilterMode.BOTH
    elif(name == "yes"):
        return parcoursprog.DiseaseFilterMode.YES
    elif(name == "no"):
        return parcoursprog.DiseaseFilterMode.NO
    raise KeyError("Name " + name + " is not a filter DiseaseFilterMode value")

def nodetreeHandler(self, data):
        "Handler of a nodetree message"
        print("nodetreee_handler", data)

        ctx     = self._ctx.currentContext
        params  = data["params"]
        reset   = bool(data.get("reset", False))
        process = bool(data.get("process", True))

        # only change the values when reseting
        for param, value in params.items():
            if param == "reset":
                assert reset == bool(value)
            elif param == "duration_type":
                ctx.duration_type = value
            elif param == "duration":
                ctx.min_duration = value[0]
                ctx.max_duration = value[1]
            elif param == "age":
                ctx.min_age = value[0]
                ctx.max_age = value[1]
            elif param == "interruption_duration":
                ctx.interruption_duration = value
            elif param == "depth":
                ctx.max_depth = value
            elif param == "quantum":
                ctx.quantum = value
            elif param == "prune_threshold":
                ctx.prune_threshold = value
            elif param == "aggregate_coarse_level":
                ctx.aggregate_coarse_level = value
            elif param == "hidden_items":
                ctx.hidden_items = value
            elif param == "align_sequence":
                ctx.align_sequence = value
            elif param == "align_sequence_by":
                ctx.align_sequence_by = value
            elif param == "diseases":
                for disease in value:
                    try:
                        ctx.disease_filter[list(ctx.diseases).index(disease["name"])] = str_to_DiseaseFilterMode(disease["status"])
                    except:
                        logging.warning("Disease %s or status %s is not a valid key", disease["name"], disease["status"])
            else:
                logging.warning("invalid parameter %s: %s", param, value)

        if reset or process:
            ctx.process_next(reset)
            ctx.update_view_data()
            ret = ctx.print().strip()
            if ret == '':
                ret = 'null'
            else: #Add additional information. This is "hacky" but works well on well-formed JSON object
                ret = f'{ret[:-1]}, \"reset\": {"true" if reset else "false"}}}'
            self.send({'data': '{"type": "nodetree",' +
                              f'"event_id": {data["event_id"]},' +
                               '"data": '+ret+"}"})


def distributionHandler(self, data):
        "Handler of a distribution message"
        print("distributionHandler", data)
        ctx = self._ctx.currentContext
        ret = {}
        node_id = int(data["node_id"])
        
        ret["age"]      = json.loads(ctx.print_histogram(parcoursprog.HistogramToPrint.AGE, node_id))
        ret["duration"] = json.loads(ctx.print_histogram(parcoursprog.HistogramToPrint.DURATION, node_id))
        ret["cum_duration"] = json.loads(ctx.print_histogram(parcoursprog.HistogramToPrint.CUM_DURATION, node_id))
        ret["disease"]  = json.loads(ctx.print_barchart(parcoursprog.BarchartToPrint.DISEASE, node_id))
        ret["nbValues"] = ctx.output_graph.nodes[node_id].count

        histogram_type = data.get("histogram_type", "hour")
        print(histogram_type)
    
        if histogram_type == "hour":
            ret["admission_date"] = json.loads(ctx.print_histogram(parcoursprog.HistogramToPrint.ADMISSION_DATE_HOUR, node_id))
        elif histogram_type == "day":
            ret["admission_date"] = json.loads(ctx.print_histogram(parcoursprog.HistogramToPrint.ADMISSION_DATE_DAY, node_id))
        elif histogram_type == "month":
            ret["admission_date"] = json.loads(ctx.print_histogram(parcoursprog.HistogramToPrint.ADMISSION_DATE_MONTH, node_id))
        elif histogram_type == "day_week":
            ret["admission_date"] = json.loads(ctx.print_histogram(parcoursprog.HistogramToPrint.ADMISSION_DATE_DAY_WEEK, node_id))
        else:
            raise ValueError(f"Invalid histogram_type: {histogram_type}")

        self.send({'data': json.dumps({
                               "type": "distribution",
                               "data": ret,
                               "event_id": data["event_id"]
                           })
        })


async def comparison_handler(websocket, data):
    client = state_context[websocket]
    params = data["params"]
    if params["stop"] == True:
        client.stopComparison()

    ctx_index = params["ctx_index"]
    if ContextIndex(ctx_index) == ContextIndex.COMPARISON_CONTEXT and not client.comparisonContext:
        client.startComparison()
    client.currentContext = client.contexts[ctx_index]

def init_context(websocket):
    state_context[websocket] = ParcoursvisClient(data_directory)

async def handler(websocket, path):
    print(websocket, "connection established")
    connected.add(websocket)
    init_context(websocket)
    try:
        async for message in websocket:
            data = json.loads(message)
            action = data.get("action")
            if action == "node_tree":
                # print("nodetree requested")
                await nodetree_handler(websocket, data)
            elif action == "distribution":
                # print("distribution requested")
                await distribution_handler(websocket, data)
            elif action == "comparison":
                await comparison_handler(websocket, data)
            else:
                logging.error(f"unsupported event: {data}")
    finally:
        connected.remove(websocket)
        del state_context[websocket]

async def main():
    async with websockets.serve(handler, "0.0.0.0", 5678):
        await asyncio.Future() #Runforever, see https://websockets.readthedocs.io/en/stable/

if __name__ == '__main__':
    if len(sys.argv) > 1:
        data_directory = sys.argv[1]
    assert os.path.isfile(os.path.join(data_directory, "data.bin"))
    assert os.path.isfile(os.path.join(data_directory, "data.map"))
    assert os.path.isfile(os.path.join(data_directory, "indi.bin"))
    print(f"# using data from directory {data_directory}")
    asyncio.run(main())

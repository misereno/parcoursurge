FROM python:3.10-slim

RUN apt-get update
RUN apt-get install -y --no-install-recommends g++ make bzip2

COPY compile.sh /
COPY requirements.txt /

RUN pip install -r requirements.txt

CMD ["/bin/bash", "/compile.sh"]
